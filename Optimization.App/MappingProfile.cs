﻿using AutoMapper;
using Optimization.Data;
using Optimization.App.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.App
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ProjectEntity, Project>()
                .ForMember(proj=> proj.Configuration, map => 
                map.ResolveUsing(proj => ProjectConfiguration.FromJson(proj.Configs)));

            CreateMap<Project, ProjectEntity>()
                .ForMember(proj => proj.Configs, map =>
                 map.ResolveUsing(proj => ProjectConfiguration.ToJsonString(proj.Configuration)));

        }
    }
}
