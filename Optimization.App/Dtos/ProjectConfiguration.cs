﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Optimization.App.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Optimization.App.Dtos
{
    public class ProjectConfiguration
    {
        public string Filename;
        public DateTime? Timestamp;

        public List<Building> Buildings;

        public string FuelType;

        public int? ChilledWater_TWS;
        public int? ChilledWater_TWR;

        public int? Npv;
        public int? Capex;
        public int? Co2;
        public int? Lcoe;
        public int? Opex;

        public int? InterestRate;
        public int? Lifetime;

        /// <summary>
        /// Parse Project Configuration from Json
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static ProjectConfiguration FromJson(string json)
        {
            ProjectConfiguration result = JsonConvert.DeserializeObject<ProjectConfiguration>(json, JsonSerializerConfigs.WithNullableJson);
            return result;
        }

        /// <summary>
        /// Serialize Project Configuration to Json string
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static string ToJsonString(ProjectConfiguration config)
        {
            var result = JsonConvert.SerializeObject(config, JsonSerializerConfigs.WithNullableJson);
            return result;
        }
    }
}