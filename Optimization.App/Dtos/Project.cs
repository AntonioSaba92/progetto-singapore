﻿using AutoMapper;
using Optimization.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.App.Dtos
{
    /// <summary>
    /// </summary>
    public class Project
    {
        public string Id { get; set; }

        public DateTime? CreationTimestamp { get; set; }

        public DateTime? UpdateTimestamp { get; set; }

        public string Author { get; set; }

        public ProjectConfiguration Configuration { get; set; }
    }
}
