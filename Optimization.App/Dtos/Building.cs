﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Optimization.App.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.App.Dtos
{
    public class Building
    {
        public string BuildingName;
        public string BuildingType;

        public int? Height;
        public int? Levels;
        public int? Area;

        public string Location;

        /// <summary>
        /// Parse Building from Json
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<Building> FromJson(string json)
        {
            List<Building> result = JsonConvert.DeserializeObject<List<Building>>(json, JsonSerializerConfigs.WithNullableJson);
            return result;
        }
    }
}
