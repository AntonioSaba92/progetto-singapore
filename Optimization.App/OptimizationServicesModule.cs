﻿using Ninject;
using Ninject.Modules;
using Optimization.App.Interfaces;
using Optimization.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.App
{
    public class OptimizationServicesModule : NinjectModule
    {
        private INinjectModule dataModule;

        public OptimizationServicesModule(INinjectModule module)
        {
            this.dataModule = module;
        }

        public override void Load()
        {
            AutoMapperConfiguration.Configure();

            Kernel.Load(this.dataModule);

            Kernel.Bind<IProjectService>().To<ProjectService>();
        }
    }
}
