﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.App.Utility
{
    public class DateTimeUtils
    {
        #region LocalTime Conversion
        /// <summary>
        /// Convert all UTC DateTime, parsed from json, in local time
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static TObject ConvertToLocalDateTime<TObject>(TObject target)
        {
            PropertyInfo[] properties = typeof(TObject).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.PropertyType == typeof(DateTime?) || property.PropertyType == typeof(DateTime))
                {
                    DateTime? date = (DateTime?)property.GetValue(target);
                    if (date.HasValue)
                    {
                        property.SetValue(target, date.Value.ToLocalTime());
                    }
                }
            }
            return target;
        }
        #endregion
    }
}
