﻿using Newtonsoft.Json;

namespace Optimization.App.Configurations
{
    public class JsonSerializerConfigs
    {
        public static JsonSerializerSettings WithNullableJson = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };
    }
}
