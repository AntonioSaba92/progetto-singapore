﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using Optimization.App.Dtos;
using Optimization.App.Interfaces;
using Optimization.App.Utility;
using Optimization.Data;
using Optimization.Data.Interfaces;

namespace Optimization.App.Services
{
    public class ProjectService : IProjectService
    {
        private IUnitOfWork uow;
        private Repository<ProjectEntity> repository;

        public ProjectService(IUnitOfWork uow)
        {
            this.uow = uow;
            this.repository = this.uow.ProjectRepository();
        }

        /// <summary>
        /// Get All Existing Projects from DB
        /// </summary>
        /// <returns></returns>
        public List<Project> GetAllProjects()
        {
            var entities = this.repository.GetAll().ToList();
            var projects = Mapper.Map<List<Dtos.Project>>(entities);
            return projects;
        }

        /// <summary>
        /// Get All Existing Projects from DB
        /// </summary>
        /// <returns></returns>
        public Project GetProjectById(string projectId)
        {
            var entity = this.repository.GetByID(projectId);
            var project = Mapper.Map<Project>(entity);
            return project;
        }

        /// <summary>
        /// Create new Project 
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public Project Create(Project project)
        {
            // set timestamps
            if (project.CreationTimestamp.HasValue == false)
            {
                project.CreationTimestamp = DateTime.Now;
            }
            project.UpdateTimestamp = DateTime.Now;
            
            // convert to local datetime
            project = DateTimeUtils.ConvertToLocalDateTime(project);
            project.Configuration = DateTimeUtils.ConvertToLocalDateTime(project.Configuration);

            // insert db
            var entity = Mapper.Map<ProjectEntity>(project);
            entity.Id = Guid.NewGuid().ToString();
            
            this.repository.Insert(entity);
            this.uow.Save();

            project = this.GetProjectById(entity.Id);
            return project;
        }

        /// <summary>
        /// Update existing Project 
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public Project Update(Project project)
        {
            // Handle Immutable records
            var stored = this.GetProjectById(project.Id);
            project.CreationTimestamp = project.CreationTimestamp;

            // update timestamp
            project.UpdateTimestamp = DateTime.Now;

            // convert to local datetime
            project = DateTimeUtils.ConvertToLocalDateTime(project);
            project.Configuration = DateTimeUtils.ConvertToLocalDateTime(project.Configuration);

            // update db
            var entity = Mapper.Map<ProjectEntity>(project);
            
            this.repository.Update(entity);
            this.uow.Save();

            project = this.GetProjectById(entity.Id);

            return project;
        }

        /// <summary>
        /// Converte un oggetto di tipo "Project" in MemoryStream formattato in XML
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public MemoryStream ExportFile(ProjectConfiguration project)
        {
            var serializer = new XmlSerializer(typeof(ProjectConfiguration));
            MemoryStream writer = new MemoryStream();

            serializer.Serialize(writer, project);
            return writer;
        }

        /// <summary>
        /// Carica un file progetto Xml
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public ProjectConfiguration ImportFile(string filepath)
        {
            // TODO: validare xml schema del file di progetto

            var project = new ProjectConfiguration();
            var deserializer = new XmlSerializer(typeof(ProjectConfiguration));
            using (var stream = new StreamReader(filepath))
            {
                project = (ProjectConfiguration)deserializer.Deserialize(stream);
            }
            return project;
        }

    }
}
