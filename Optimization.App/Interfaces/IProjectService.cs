﻿using Optimization.App.Dtos;
using Optimization.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Optimization.App.Interfaces
{
    public interface IProjectService
    {
        List<Project> GetAllProjects();
        Project GetProjectById(string projectId);

        Project Create(Project project);
        Project Update(Project project);

        MemoryStream ExportFile(ProjectConfiguration project);
        ProjectConfiguration ImportFile(string filepath);
    }
}
