﻿using Optimization.Api.Models;
using Optimization.App.Dtos;
using Optimization.App.Interfaces;
using Optimization.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;

namespace Optimization.Controllers
{
    [RoutePrefix("api")]
    public class ProjectController : ApiController
    {
        private IProjectService projectService;

        public ProjectController(IProjectService ps)
        {
            this.projectService = ps;
        }

        /// <summary>
        /// Get all projects
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("projects")]
        public IHttpActionResult GetAll()
        {
            var projects = this.projectService.GetAllProjects();
            return Ok(projects);
        }

        /// <summary>
        /// Get Project By Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("project")]
        public IHttpActionResult GetById(string projectId)
        {
            var project = this.projectService.GetProjectById(projectId);
            return Ok(project);
        }

        [HttpPost]
        [Route("project/create")]
        public IHttpActionResult SaveProject(Project project)
        {
            project = this.projectService.Create(project);
            return Ok(project);
        }

        [HttpPost]
        [Route("project/update")]
        public IHttpActionResult UpdateProject(Project project)
        {
            project = this.projectService.Update(project);
            return Ok(project);
        }

        #region Import/Export Methods
        [HttpGet]
        [Route("project/export")]
        public HttpResponseMessage ExportProjectById(string projectId)
        {
            try
            {
                // caricamento progetto
                var project = this.projectService.GetProjectById(projectId);
                var model = project.Configuration;

                // export in formato xml
                var stream = this.projectService.ExportFile(model);
                
                // download
                var result = Build_Project_XML_HttpResponse(stream);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Obsoleto?
        // Esporta un progetto in xml a partire da modello passato come parametro
        //[HttpPost]
        //[Route("project/export")]
        //public HttpResponseMessage ExportProject([FromBody] ProjectConfiguration model)
        //{
        //    try
        //    {
        //        var stream = this.projectService.ExportFile(model);
        //        var result = Build_Project_XML_HttpResponse(stream);
        //        return result;
        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        [HttpPost]
        [Route("project/import")]
        [MultiPartContentValidation]
        public async Task<IHttpActionResult> ImportProjectAsync()
        {
            try
            {
                string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $@"App_Data");
                MultipartFormDataStreamProvider streamProvider;
                streamProvider = new MultipartFormDataStreamProvider(folder);
                await Request.Content.ReadAsMultipartAsync(streamProvider);

                var tmp_filename = streamProvider.FileData.Select(entry => entry.LocalFileName).FirstOrDefault();

                var project = this.projectService.ImportFile(tmp_filename);
                if (File.Exists(tmp_filename))
                {
                    File.Delete(tmp_filename);
                }
                return Ok(project);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Incapsula un Progetto esportato come MemoryStream in un HttpResponseMessage scaricabile
        /// </summary>
        /// <param name="project_as_stream"></param>
        /// <returns></returns>
        private HttpResponseMessage Build_Project_XML_HttpResponse(MemoryStream project_as_stream)
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(project_as_stream.ToArray())
            };

            var today = DateTime.Today;
            result.Content.Headers.ContentDisposition =
                new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"project_{today.ToString("yyyyMMdd")}.xml"
                };

            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        #endregion
    }
}
