﻿using Ninject;
using Optimization.Data;
using Optimization.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.Tests.Ninject
{
    public class NinjectCommon
    {

        private static IKernel kernel;

        public static IKernel Kernel 
        {
            get {
                if (kernel == null)
                {
                    kernel = new StandardKernel();
                    RegisterServices(kernel);
                }

                return kernel;
            }
        }

        #region Methods
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Load(new OptimizationServicesModule(new DataModule()));
        }

        #endregion

    }
}
