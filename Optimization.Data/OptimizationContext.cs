namespace Optimization.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Optimization.Data.Interfaces;

    public partial class OptimizationContext : DbContext, IDbContext
    {
        public OptimizationContext() : base("name=dbcontext")
        {
        }

        public virtual DbSet<ProjectEntity> Project { get; set; }

        public DbContext Context()
        {
            return this;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
