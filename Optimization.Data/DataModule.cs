﻿using Ninject.Modules;
using Ninject.Web.Common;
using Optimization.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.Data
{
    public class DataModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            Bind<IDbContext>().To<OptimizationContext>().InRequestScope();
        }
    }
}
