﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.Data.Interfaces
{
    public interface IDbContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Commit delle modifiche pendenti
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        DbContext Context();

        void Dispose();
    }
}
