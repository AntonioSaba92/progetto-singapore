﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimization.Data.Interfaces
{
    public interface IUnitOfWork
    {
        Repository<ProjectEntity> ProjectRepository();
        void Save();
    }
}
