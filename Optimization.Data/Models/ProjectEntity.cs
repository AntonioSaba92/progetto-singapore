namespace Optimization.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Project")]
    public partial class ProjectEntity
    {
        [StringLength(64)]
        public string Id { get; set; }

        public DateTime CreationTimestamp { get; set; }

        public DateTime UpdateTimestamp { get; set; }

        [StringLength(50)]
        public string Author { get; set; }

        public string Configs { get; set; }
    }
}
