﻿using Optimization.Data.Interfaces;
using System;

namespace Optimization.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbContext context;
        private Repository<ProjectEntity> projectRepository;

        public UnitOfWork(IDbContext ctx)
        {
            this.context = ctx;
        }

        #region Repositories
        /// <summary>
        /// </summary>
        public Repository<ProjectEntity> ProjectRepository()
        {
            if (this.projectRepository == null)
            {
                this.projectRepository = new Repository<ProjectEntity>(context);
            }
            return this.projectRepository;
        }

        #endregion

        #region Methods



        public void Save()
        {
            using (var dbContextTransaction = context.Context().Database.BeginTransaction())
            {
                try
                {
                    context.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch(Exception ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
