import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {


  public home = true

  constructor() { }

  closeResult: string;   // per la modal di aggiunta nuovo progetto

  ngOnInit() {
     document.getElementById('build-sel').addEventListener('click',reloadBuild)
    document.getElementById('home').addEventListener('click',reloadBuild)

    document.getElementById('opt-sel').addEventListener('click',reload)
    document.getElementById('opt-res').addEventListener('click',reload)
    document.getElementById('inp-sel').addEventListener('click',reload)

    function reloadBuild(){
      window.location.reload()
    }


    function reload(){
      // window.location.reload()
      $('#master').toggleClass('collapsed');
      $('#masterSubmenu').removeClass('show');
      $('#scheduling').toggleClass('collapsed');
      $('#schedSubmenu').removeClass('show');
      $('#sidebar').toggleClass('active');
      $('#linea-logo').toggleClass('linea-attiva');
      $('#scritta-logo').toggleClass('scritta-attiva');
  
    }
    
    $(document).ready(function () {

      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
          $('#revolt-logo').toggleClass('active');
          $('#overlay').toggleClass('attivoo');
          $('#sidebarCollapse').toggleClass('coll-attivo');
          $('#linea-logo').toggleClass('linea-attiva');
          $('#scritta-logo').toggleClass('scritta-attiva');
          $('#master').toggleClass('collapsed');
          $('#masterSubmenu').removeClass('show');
          $('#scheduling').toggleClass('collapsed');
          $('#schedSubmenu').removeClass('show');
      
      
          
      });
  
    let sidebar = document.getElementById('sidebar');

    if(sidebar.className == "active"){
        $('#master').on('click', function () {
        $('#sidebar').removeClass('active');
        $('#revolt-logo').addClass('active');
        // $('#overlay').toggleClass('attivoo');
        $('#sidebarCollapse').addClass('coll-attivo');
        $('#linea-logo').addClass('linea-attiva');
        $('#scritta-logo').addClass('scritta-attiva');
        
    });
    $('#scheduling').on('click', function () {
      $('#sidebar').removeClass('active');
      $('#revolt-logo').addClass('active');
      // $('#overlay').toggleClass('attivoo');
      $('#sidebarCollapse').addClass('coll-attivo');
      $('#linea-logo').addClass('linea-attiva');
      $('#scritta-logo').addClass('scritta-attiva');
      
  });
    }


    //   $('#master').on('click', function () {
    //     $('#sidebar').toggleClass('active');
    //     $('#revolt-logo').toggleClass('active');
    //     $('#overlay').toggleClass('attivoo');
    //     // $('#sidebarCollapse').toggleClass('coll-attivo');
    //     $('#linea-logo').toggleClass('linea-attiva');
    //     $('#scritta-logo').toggleClass('scritta-attiva');
        
    // });
  });


  // let masterbtn = document.getElementById('master');
  // masterbtn.addEventListener('click',openSide);

  // function openSide(){
  //   document.getElementById('sidebar').className = "";
  // }

  var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function() {
    this.parentElement.querySelector(".nested").classList.toggle("active");
    this.classList.toggle("caret-down");
  });
}
   }
  



  addCard(){
    let name = (<HTMLInputElement>document.getElementById('nameOfProj')).value;
    let newCard =  document.createElement('div');
    newCard.className = 'card';
    let newCardBody = document.createElement('div');
    newCardBody.className = 'card-body card-body-custom';
    newCardBody.style.outline="none";
    newCard.appendChild(newCardBody);
    let newh5 = document.createElement('h5');
    newh5.className = 'card-title'
    let textTitle = document.createTextNode(name);
    newh5.appendChild(textTitle);
    newCardBody.appendChild(newh5);
    let newCol = document.createElement('div');
    newCol.className = 'col-lg-4';
    newCol.appendChild(newCard);
    let rowDash = document.getElementById('row-dash');
    rowDash.appendChild(newCol);
    let p = document.createElement('p');
    p.className = "card-text";
    let ptext = document.createTextNode('Project Description');
    p.appendChild(ptext);
    newCardBody.appendChild(p);
    let open = document.createElement('button');
    let openText = document.createTextNode('Go');
    open.appendChild(openText);
    open.className = "btn btn-success";
    newCardBody.appendChild(open);
  
  }
  

}

