import { BUILDINGS } from "./../buildings";
import { Component, OnInit } from "@angular/core";
import { map, numberFormat, color } from "highcharts";
import * as Highcharts from "highcharts";
import { Location } from "@angular/common";
import { DataService } from "../data.service";
import { Building } from "../building";
import { PdfCreatorService } from "../pdf-creator.service";

declare var OSMBuildings: any;

const axios = require("axios");

declare var require: any;

@Component({
  selector: "app-map-dashboard",
  templateUrl: "./map-dashboard.component.html",
  styleUrls: ["./map-dashboard.component.scss"]
})
export class MapDashboardComponent implements OnInit {
  message: string;
  number = 1;

  edifici: Building[];
  building: Building;
  nome: string;
  altezza: string;

  constructor(private data: DataService, private pdf: PdfCreatorService) {}

  transferInput() {
    this.number = this.number + 1;

    let nome = <HTMLInputElement>document.getElementById("nome");
    let tipo = <HTMLInputElement>document.getElementById("tipo");
    let altezza = <HTMLInputElement>document.getElementById("altezza");
    let piani = <HTMLInputElement>document.getElementById("piani");
    let name = nome.value.toString();
    let type = tipo.value.toString();
    let height = altezza.value.toString();
    let levels = piani.value.toString();

    (this.building = {
      name: name,
      type: type,
      height: height,
      levels: levels
    }),
      BUILDINGS.push(this.building);
    console.log(BUILDINGS);

    this.data.changeBuilding(BUILDINGS);

    nome.value = "";
    tipo.value = "";
    altezza.value = "";
    piani.value = "";
  }

  newMessage() {
    this.data.changeMessage("Hello from Sibling");
  }

  ngOnInit() {
    httpGetAsync("http://localhost:9999/api/project/export", response => {
      var json = JSON.parse(response);
      console.log(json);
    });
    // $.get("http://localhost:9999/api/project/test", function(data) {
    //   $(".result").html(data);
    //   alert("Load was performed.");
    // });
    // httpGetAsync("http://localhost:9999/api/project/test", response => {
    //   var json = JSON.parse(response);
    //   console.log(json);
    // });
    localStorage.clear();

    this.data.currentMessage.subscribe(message => (this.message = message));

    //---------------------------------------------- PER SCARICARE PDF DINAMICO------------------------------------------------------

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    document.getElementById("take-screen").addEventListener("click", aggImm);

    var index = 0;
    function aggImm() {
      index++;
      localStorage.setItem(
        "ultimoScreen-" + index,
        immagini[immagini.length - 1]
      );
      console.log(immagini[immagini.length - 1]);
    }

    var map = new OSMBuildings({
      container: "map",

      position: { latitude: 1.29027, longitude: 103.851959 },

      zoom: 16,

      minZoom: -200,

      maxZoom: 200,

      effects: ["shadows"],

      attribution:
        '© Data <a href="https://openstreetmap.org/copyright/">OpenStreetMap</a> © Map <a href="https://mapbox.com/">Mapbox</a> © 3D <a href="https://osmbuildings.org/copyright/">OSM Buildings</a>'
    });

    map.addMapTiles(
      "https://api.mapbox.com/styles/v1/osmbuildings/cjt9gq35s09051fo7urho3m0f/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYXNhYmF0ZWxsaSIsImEiOiJjazE5MmdjbXQwMWhoM2dwaTluYWhjaG1yIn0.VVksz39OUVbedu4-Ln1aiA"
    ),
      map.addGeoJSONTiles(
        "https://{s}.data.osmbuildings.org/0.2/anonymous/tile/{z}/{x}/{y}.json"
      );

    // OGNI VOLTA CHE SI PREME SU UN EDIFICIO, VIENE FATTO UNO SCREENSHOT CHE SI SALVA TRAMITE LOCAL STORAGE------------
    var idScreen = 0;
    var immagini = [];

    map.on("pointerup", e => {
      idScreen = idScreen + 1;
      // console.log(idScreen);
      let feat = e.features[0].id;
      var url =
        "http://overpass-api.de/api/interpreter?data=[out:json];(way(" +
        feat +
        ");node(w););out;";
      // console.log(url);
      httpGetAsync(url, response => {
        var json = JSON.parse(response);
        // console.log(response);
        var lat = json.elements[0].lat;
        var lon = json.elements[0].lon;
        var idscreenshot = idScreen.toString();
        var imageScreen = document.createElement("img");
        var width = document.createAttribute("width");
        width.value = "600";
        imageScreen.id = "screen-" + idscreenshot;
        imageScreen.setAttributeNode(width);
        imageScreen.src =
          "https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/static/pin-l-commercial+285A98(" +
          lon +
          "," +
          lat +
          ")/" +
          lon +
          "," +
          lat +
          ",17,0,29/600x300@2x?access_token=pk.eyJ1IjoiYXNhYmF0ZWxsaSIsImEiOiJjazE5MmdjbXQwMWhoM2dwaTluYWhjaG1yIn0.VVksz39OUVbedu4-Ln1aiA";

        // console.log(imageScreen);
        // document.getElementById("main").appendChild(imageScreen);

        localStorage.setItem("screen-" + idScreen, imageScreen.outerHTML);

        // console.log(imageScreen.outerHTML);

        immagini.push(imageScreen.outerHTML);
        console.log(immagini);
      });
    });

    map.on("pointerup", e => {
      // if none, remove any previous selection and return
      if (!e.features) {
        map.highlight(feature => {});
        return;
      }

      let feat = e.features[0].id;

      // store id's from seleted items...
      const featureIDList = e.features.map(feature => feature.id);

      // console.log(featureIDList)
      var url =
        "http://overpass-api.de/api/interpreter?data=[out:json];(way(" +
        feat +
        ");node(w););out;";
      // console.log(url);
      httpGetAsync(url, response => {
        var json = JSON.parse(response);
        // console.log(response);

        //  console.log(json.elements[json.elements.length-1].tags);
        let height = json.elements[json.elements.length - 1].tags["height"];
        let type = json.elements[json.elements.length - 1].tags["building"];
        let levels =
          json.elements[json.elements.length - 1].tags["building:levels"];

        let tipo = <HTMLInputElement>document.getElementById("tipo");
        tipo.value = type;
        let altezza = <HTMLInputElement>document.getElementById("altezza");
        altezza.value = height;
        let piani = <HTMLInputElement>document.getElementById("piani");
        piani.value = levels;

        let height2 = json.elements[json.elements.length - 1].tags["height"];
        let type2 = json.elements[json.elements.length - 1].tags["building"];
        let levels2 =
          json.elements[json.elements.length - 1].tags["building:levels"];

        let tipo2 = <HTMLInputElement>(
          document.getElementById("type-info-val-2")
        );
        tipo2.value = type2;
        let altezza2 = <HTMLInputElement>(
          document.getElementById("height-info-val-2")
        );
        altezza2.value = height2;
        let piani2 = <HTMLInputElement>(
          document.getElementById("levels-info-val-2")
        );
        piani2.value = levels2;
      });

      // ...then is is faster: set highlight color for matching features
      map.highlight(feature => {
        if (featureIDList.indexOf(feature.id) > -1) {
          return "red";
        }
      });
    });

    // this.data.currentName.subscribe(nome => this.nome = nome);
    // this.data.currentHeight.subscribe(altezza => this.altezza = altezza)

    document.getElementById("homeSel").style.fill = "white";
    document.getElementById("masterSel").style.fill = "#196eed";
    document.getElementById("schedSel").style.fill = "white";

    document.getElementById("inp-sel").style.color = "white";
    document.getElementById("opt-sel").style.color = "white";
    document.getElementById("opt-res").style.color = "white";
    document.getElementById("build-sel").style.color = "#196eed";

    document.getElementById("electric-in").addEventListener("click", upImEl);
    document.getElementById("electric-in-2").addEventListener("click", upImEl2);

    function upImEl() {
      document.getElementById("electric-btn").style.display = "none";
      document.getElementById("electric-prof").style.display = "block";
    }

    function upImEl2() {
      document.getElementById("electric-btn-2").style.display = "none";
      document.getElementById("electric-prof-2").style.display = "block";
    }

    document.getElementById("cooling-in").addEventListener("click", upImCool);
    document
      .getElementById("cooling-in-2")
      .addEventListener("click", upImCool2);

    function upImCool() {
      document.getElementById("cooling-btn").style.display = "none";
      document.getElementById("cooling-prof").style.display = "block";
    }

    function upImCool2() {
      document.getElementById("cooling-btn-2").style.display = "none";
      document.getElementById("cooling-prof-2").style.display = "block";
    }

    document.getElementById("back-map").addEventListener("click", reload);
    function reload() {
      window.location.reload();
    }

    document.getElementById("back-map-2").addEventListener("click", reload);

    function httpGetAsync(theUrl, callback) {
      var xmlHttp = new XMLHttpRequest();
      xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
          callback(xmlHttp.responseText);
      };
      xmlHttp.open("GET", theUrl, true); // true for asynchronous
      xmlHttp.send(null);
    }

    // Get location form
    var locationForm = document.getElementById("location-form");
    locationForm.addEventListener("submit", geocode);

    function geocode(e) {
      // Prevent actual submit
      e.preventDefault();
      var location = (<HTMLInputElement>(
        document.getElementById("location-input")
      )).value;

      // trasformiamo il valore digitato sulla search bar in coordinate
      axios
        .get(
          "http://www.mapquestapi.com/geocoding/v1/address?key=xoAmtnAepaQ5lVk9136dQRlhtlGNI8xl&location=" +
            location,
          {}
        )
        .then(function(response) {
          // Log full response
          console.log(response);

          // Geometry: troviamo la latitudine e longitudine dal dato cercato
          var lat = response.data.results[0].locations[0].latLng.lat;
          var lng = response.data.results[0].locations[0].latLng.lng;

          map.position.latitude = lat;
          map.position.longitude = lng;

          map.on("pointerup", e => {
            // if none, remove any previous selection and return
            if (!e.features) {
              map.highlight(feature => {});
              return;
            }

            let feat = e.features[0].id;
            idScreen++;

            // store id's from seleted items...
            const featureIDList = e.features.map(feature => feature.id);
            // console.log(featureIDList)
            var url =
              "http://overpass-api.de/api/interpreter?data=[out:json];(way(" +
              feat +
              ");node(w););out;";
            // console.log(url);
            httpGetAsync(url, response => {
              var json = JSON.parse(response);
              console.log(response);

              var latMarker = json.elements[0].lat;
              var lonMarker = json.elements[0].lon;
              var idscreenshot = idScreen.toString();
              var imageScreen = document.createElement("img");
              var width = document.createAttribute("width");
              width.value = "600";
              imageScreen.id = "screen-" + idscreenshot;
              imageScreen.setAttributeNode(width);
              imageScreen.src =
                "https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/static/pin-l-commercial+285A98(" +
                lonMarker +
                "," +
                latMarker +
                ")/" +
                lng +
                "," +
                lat +
                ",17,0,29/600x300@2x?access_token=pk.eyJ1IjoiYXNhYmF0ZWxsaSIsImEiOiJjazE5MmdjbXQwMWhoM2dwaTluYWhjaG1yIn0.VVksz39OUVbedu4-Ln1aiA";
              localStorage.setItem("screen-" + idScreen, imageScreen.outerHTML);

              // console.log(imageScreen);
              // document.getElementById("main").appendChild(imageScreen);
              //  console.log(json.elements[json.elements.length-1].tags);
              let height =
                json.elements[json.elements.length - 1].tags["height"];
              let type =
                json.elements[json.elements.length - 1].tags["building"];
              let levels =
                json.elements[json.elements.length - 1].tags["building:levels"];

              let tipo = <HTMLInputElement>(
                document.getElementById("type-info-val")
              );
              tipo.value = type;
              let altezza = <HTMLInputElement>(
                document.getElementById("height-info-val")
              );
              altezza.value = height;
              let piani = <HTMLInputElement>(
                document.getElementById("levels-info-val")
              );
              piani.value = levels;

              let height2 =
                json.elements[json.elements.length - 1].tags["height"];
              let type2 =
                json.elements[json.elements.length - 1].tags["building"];
              let levels2 =
                json.elements[json.elements.length - 1].tags["building:levels"];

              let tipo2 = <HTMLInputElement>(
                document.getElementById("type-info-val-2")
              );
              tipo2.value = type2;
              let altezza2 = <HTMLInputElement>(
                document.getElementById("height-info-val-2")
              );
              altezza2.value = height2;
              let piani2 = <HTMLInputElement>(
                document.getElementById("levels-info-val-2")
              );
              piani2.value = levels2;
            });

            // ...then is is faster: set highlight color for matching features
            map.highlight(feature => {
              if (featureIDList.indexOf(feature.id) > -1) {
                return "red";
              }
            });
          });
        });
    } //fine function geocode

    //     let button = document.querySelector('.button-save-build');
    //     button.addEventListener('click', function() {

    // });

    Highcharts.chart("buildA", this.options);
    Highcharts.chart("buildB", this.options2);
  } // fine ngOninit

  public cardColor = "#212B46";
  public options: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: "areaspline",
      zoomType: "xy"
    },

    plotOptions: {
      areaspline: {
        marker: {
          enabled: false
        }
      }
    },
    legend: {
      layout: "horizontal",
      align: "center",
      verticalAlign: "bottom",
      itemStyle: {
        color: "white",
        fontWeight: "bold"
      }
    },

    title: {
      style: {
        color: "white"
      },
      text: ""
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: "time [min]"
      },

      categories: [
        "0.5",
        "1",
        "1.5",
        "2",
        "2.5",
        "3",
        "3.5",
        "4",
        "4.5",
        "5",
        "5.5",
        "6",
        "6.5",
        "7",
        "7.5",
        "8",
        "8.5",
        "9",
        "9.5",
        "10",
        "10.5",
        "11",
        "11.5",
        "12",
        "12.5",
        "13",
        "13.5",
        "14",
        "14.5",
        "15",
        "15.5",
        "16",
        "16.5",
        "17",
        "17.5",
        "18",
        "18.5",
        "19",
        "19.5",
        "20",
        "20.5",
        "21",
        "21.5",
        "22",
        "22.5",
        "23",
        "23.5",
        "24"
      ]
    },

    yAxis: [
      {
        //--- Primary yAxis
        gridLineWidth: 0.2,
        lineWidth: 1,
        title: {
          text: "Electricity [kW]"
        }
      },
      {
        //--- Secondary yAxis
        lineWidth: 1,
        title: {
          text: "Cooling [RT]",
          rotation: 270
        },
        opposite: true
      }
    ],

    exporting: {
      enabled: false
    },

    credits: {
      enabled: false
    },

    series: [
      {
        lineWidth: 1.5,
        lineColor: "yellow",
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, "yellow"],
            [1, "transparent"]
          ]
        },

        name: "Electricity",
        yAxis: 0,
        data: [
          984,
          972,
          976,
          948,
          912,
          908,
          885,
          942,
          936,
          926,
          956,
          930,
          908,
          986,
          922,
          982,
          1130,
          1246,
          1330,
          1310,
          1354,
          1286,
          1406,
          1418,
          1442,
          1442,
          1372,
          1488,
          1456,
          1418,
          1420,
          1610,
          1422,
          1554,
          1382,
          1388,
          1278,
          1256,
          1184,
          1014,
          992,
          982,
          964,
          934,
          958,
          918,
          920,
          914
        ]
      },
      {
        lineWidth: 1.5,
        lineColor: "#196eed",
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, "#196eed"],
            [1, "transparent"]
          ]
        },
        name: "Cooling",
        yAxis: 1,
        data: [
          363,
          377,
          358,
          349,
          365,
          359,
          327,
          335,
          343,
          343,
          348,
          325,
          353,
          353,
          449,
          445,
          817,
          785,
          630,
          721,
          617,
          698,
          676,
          678,
          662,
          660,
          656,
          656,
          651,
          642,
          671,
          683,
          660,
          635,
          479,
          436,
          399,
          369,
          335,
          305,
          295,
          287,
          281,
          278,
          277,
          275,
          371,
          368
        ]
      }
    ]
  };

  public options2: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: "areaspline",
      zoomType: "xy"
    },

    plotOptions: {
      areaspline: {
        marker: {
          enabled: false
        }
      }
    },
    legend: {
      layout: "horizontal",
      align: "center",
      verticalAlign: "bottom",
      itemStyle: {
        color: "white",
        fontWeight: "bold"
      }
    },

    title: {
      style: {
        color: "white"
      },
      text: ""
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: "time [min]"
      },

      categories: [
        "0.5",
        "1",
        "1.5",
        "2",
        "2.5",
        "3",
        "3.5",
        "4",
        "4.5",
        "5",
        "5.5",
        "6",
        "6.5",
        "7",
        "7.5",
        "8",
        "8.5",
        "9",
        "9.5",
        "10",
        "10.5",
        "11",
        "11.5",
        "12",
        "12.5",
        "13",
        "13.5",
        "14",
        "14.5",
        "15",
        "15.5",
        "16",
        "16.5",
        "17",
        "17.5",
        "18",
        "18.5",
        "19",
        "19.5",
        "20",
        "20.5",
        "21",
        "21.5",
        "22",
        "22.5",
        "23",
        "23.5",
        "24"
      ]
    },

    yAxis: [
      {
        //--- Primary yAxis
        gridLineWidth: 0.2,
        lineWidth: 1,
        title: {
          text: "Electricity [kW]"
        }
      },
      {
        //--- Secondary yAxis
        lineWidth: 1,
        title: {
          text: "Cooling [RT]",
          rotation: 270
        },
        opposite: true
      }
    ],

    exporting: {
      enabled: false
    },

    credits: {
      enabled: false
    },

    series: [
      {
        lineWidth: 1.5,
        lineColor: "yellow",
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, "yellow"],
            [1, "transparent"]
          ]
        },

        name: "Electricity",
        yAxis: 0,
        data: [
          492,
          486,
          488,
          474,
          456,
          454,
          442,
          471,
          468,
          463,
          478,
          465,
          454,
          493,
          461,
          491,
          565,
          623,
          665,
          655,
          677,
          643,
          703,
          709,
          721,
          721,
          686,
          744,
          728,
          709,
          710,
          805,
          711,
          777,
          691,
          694,
          639,
          628,
          592,
          507,
          496,
          491,
          482,
          467,
          479,
          459,
          460,
          457
        ]
      },
      {
        lineWidth: 1.5,
        lineColor: "#196eed",
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, "#196eed"],
            [1, "transparent"]
          ]
        },
        name: "Cooling",
        yAxis: 1,
        data: [
          181,
          188,
          179,
          174,
          182,
          179,
          163,
          167,
          171,
          171,
          174,
          162,
          176,
          176,
          224,
          222,
          408,
          392,
          315,
          360,
          308,
          349,
          338,
          339,
          331,
          330,
          328,
          328,
          325,
          321,
          335,
          341,
          330,
          317,
          239,
          218,
          199,
          184,
          167,
          152,
          147,
          143,
          140,
          139,
          138,
          137,
          185,
          184
        ]
      }
    ]
  };
}
