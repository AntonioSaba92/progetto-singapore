import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentDashComponent } from './content-dash.component';

describe('ContentDashComponent', () => {
  let component: ContentDashComponent;
  let fixture: ComponentFixture<ContentDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
