import { Component } from '@angular/core';
import * as Highcharts from 'highcharts';
declare var require: any;
let hisogram = require('../../../node_modules/highcharts/modules/histogram-bellcurve');
hisogram(Highcharts);


@Component({
  selector: 'app-content-dash',
  templateUrl: './content-dash.component.html',
  styleUrls: ['./content-dash.component.scss']
})
export class ContentDashComponent  {


  
  public cardColor = '#212B46';

  constructor() { }
  public options: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: 'areaspline',
      marginRight: 5,
            events: {
        load: function () {
          // set up the updating of the chart each second
          var series = this.series[0];
          setInterval(function () {
            var x = (new Date()).getTime(), // current time
                                               // y = Math.random() * (1000 - 0) + 0; 
              y = Math.random();  //
            series.addPoint([x, y], true, true);
          }, 1000);
        }
      }
    },
    plotOptions: {
      series: {
        lineWidth:1.5,
        lineColor: 'yellow',
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
              [0, 'yellow'],
              [1, 'transparent']
          ],
      }  ,
      },

  },
  
    time: {
      useUTC: false
    },
  
    title: {
      style: {
        color: 'white',
      },
       text: 'Electrical Demand'
    },

    xAxis: {
      type: 'datetime',
      tickPixelInterval: 150
    },
    yAxis: {
      title: {
        text: 'Energy [kW]'
      },
   
    },
    tooltip: {
      headerFormat: '<b>{series.name}</b><br/>',
      pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
    },
    legend: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'Data',
      data: (function () {
        // generate an array of random data
        var data = [],
          time = (new Date()).getTime(),
          i;
  
        for (i = -19; i <= 0; i += 1) {
          data.push({
            x: time + i * 1000,
            y: Math.random()
          });
        }
        return data;
      }())
    }]
    
  }

  public options2: any = {
    chart: {
      type: 'areaspline',
      height: 150,
      backgroundColor:this.cardColor,
      events: {
        load: function () {
          // set up the updating of the chart each second
          var series = this.series[0];
          setInterval(function () {
            var x = (new Date()).getTime(), // current time
                                               // y = Math.random() * (1000 - 0) + 0; 
              y = Math.random();  //
            series.addPoint([x, y], true, true);
          }, 1000);
        }
      },

    },
    plotOptions: {
    series: {
      lineWidth:1.5,
      lineColor: '#00FFFF',
      color: {
      linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
      stops: [
          [0, '#00FFFF'],
          [1, 'transparent']
      ],
  }  ,
},
  },
  
    time: {
      useUTC: false
    },
  
    title: {
      style: {
        color: 'white',
      },
       text: 'Electrical Demand'
    },

    xAxis: {
      type: 'datetime',
      tickPixelInterval: 150
    },
    yAxis: {
      title: {
        text: 'Energy [kW]'
      },

      plotLines: [{
        value: 0,
        width: 1,
        color: 'green'
      }]
    },
    tooltip: {
      headerFormat: '<b>{series.name}</b><br/>',
      pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
    },
    legend: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'Data',
      data: (function () {
        // generate an array of random data
        var data = [],
          time = (new Date()).getTime(),
          i;
  
        for (i = -19; i <= 0; i += 1) {
          data.push({
            x: time + i * 1000,
            y: Math.random()
          });
        }
        return data;
      }())
    }]
    
  }

  public options3: any = {
    chart: {
      type: 'areaspline',
      height: 150,
      backgroundColor: this.cardColor,
      events: {
        load: function () {
          // set up the updating of the chart each second
          var series = this.series[0];
          setInterval(function () {
            var x = (new Date()).getTime(), // current time
                                               // y = Math.random() * (1000 - 0) + 0; 
              y = Math.random();  //
            series.addPoint([x, y], true, true);
          }, 1000);
        }
      },
    },
    plotOptions: {
      series: {
        lineWidth:1.5,
        lineColor: '#E000E0',
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
              [0, '#E000E0'],
              [1, 'transparent']
          ],
      }  ,
      },
  },
  
    time: {
      useUTC: false
    },
  
    title: {
      style: {
        color: 'white',
      },
       text: 'Electrical Demand'
    },

    xAxis: {
      type: 'datetime',
      tickPixelInterval: 150
    },
    yAxis: {
      title: {
        text: 'Energy [kW]'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: 'green'
      }]
    },
    tooltip: {
      headerFormat: '<b>{series.name}</b><br/>',
      pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
    },
    legend: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'Data',
      data: (function () {
        // generate an array of random data
        var data = [],
          time = (new Date()).getTime(),
          i;
  
        for (i = -19; i <= 0; i += 1) {
          data.push({
            x: time + i * 1000,
            y: Math.random()
          });
        }
        return data;
      }())
    }]
    
  }

  public options4: any = {
    chart: {
      lineWidth:1.5,
      type: 'areaspline',
      height: 150,
      backgroundColor:this.cardColor,
      events: {
        load: function () {
          // set up the updating of the chart each second
          var series = this.series[0];
          setInterval(function () {
            var x = (new Date()).getTime(), // current time
                                               // y = Math.random() * (1000 - 0) + 0; 
              y = Math.random();  //
            series.addPoint([x, y], true, true);
          }, 1000);
        }
      },

    },
    plotOptions: {
      series: {
        lineColor: '#00FF00',
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
              [0, '#00FF00'],
              [1, 'transparent']
          ],
      }  ,
      },
      
  },
  
    time: {
      useUTC: false
    },
  
    title: {
      style: {
        color: 'white',
      },
       text: 'Electrical Demand'
    },

    xAxis: {
      type: 'datetime',
      tickPixelInterval: 150
    },
    yAxis: {
      title: {
        text: 'Energy [kW]'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: 'green'
      }]
    },
    tooltip: {
      headerFormat: '<b>{series.name}</b><br/>',
      pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
    },
    legend: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'Data',
      data: (function () {
        // generate an array of random data
        var data = [],
          time = (new Date()).getTime(),
          i;
  
        for (i = -19; i <= 0; i += 1) {
          data.push({
            x: time + i * 1000,
            y: Math.random()
          });
        }
        return data;
      }())
    }]
    
  }

  ngOnInit(){

    Highcharts.chart('chart', this.options);
    Highcharts.chart('chart2', this.options2);
    Highcharts.chart('chart3', this.options3);
    Highcharts.chart('chart4', this.options4);
    document.getElementById('homeSel').style.fill = "#196eed";
    document.getElementById('masterSel').style.fill = "white";
    document.getElementById('schedSel').style.fill = "white";
    // document.getElementById('summary-tab').style.color = "white"
    document.getElementById('opt-sel').style.color = "white";
    document.getElementById('build-sel').style.color = "white";
    document.getElementById('opt-res').style.color = "white";
    document.getElementById('inp-sel').style.color = "white";



  }
}
