import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSelComponent } from './input-sel.component';

describe('InputSelComponent', () => {
  let component: InputSelComponent;
  let fixture: ComponentFixture<InputSelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputSelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
