import { PdfCreatorService } from "../pdf-creator.service";
import { Component, OnInit } from "@angular/core";
import * as Highcharts from "highcharts";
import * as jsPDF from "jspdf";

@Component({
  selector: "app-optimization-results",
  templateUrl: "./optimization-results.component.html",
  styleUrls: ["./optimization-results.component.scss"]
})
export class OptimizationResultsComponent implements OnInit {
  constructor(private pdf: PdfCreatorService) {}

  cos;
  downloadPdf() {
    this.cos.pdf.createPdf();
  }

  ngOnInit() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    console.log(y);
    var a = document
      .getElementById("download")
      .addEventListener("click", this.downloadPdf);

    // document
    //   .getElementById("download-pdf")
    //   .addEventListener("click", this.downloadPdf);

    // var saved = localStorage.getItem("screen1");
    // var screen = document.createElement("div");
    // screen.innerHTML = saved;
    // console.log(screen);
    // document.getElementById("drag").appendChild(screen);

    // Default export is a4 paper, portrait, using px for units

    // var doc = new jsPDF("p", "px");
    // var titolo = document.getElementById("im-titolo");
    // var screen = document.getElementById("screen-1");
    // doc.addImage(titolo, "PNG", 35, 10, 380, 250, "SLOW", 0);
    // doc.addImage(screen, "JPG", 35, 50, 380, 250, "SLOW", 0);
    // var name = "Alessandro Romagnoli";
    // var date = new Date();
    // var d = date.getDate();
    // var m = date.getMonth();
    // m = m + 1;
    // var y = date.getFullYear();
    // console.log(m + d + y);
    // doc.page = 1;
    // doc.text(name, 170, 280);
    // doc.text(d + "/" + m + "/" + y, 200, 310);
    // doc.save("a4.pdf");

    document.getElementById("dragdrop").addEventListener("click", showDrag);

    function showDrag() {
      document.getElementById("drag").style.display = "block";
      document.getElementById("e-load").style.display = "none";
      document.getElementById("c-load").style.display = "none";
      document.getElementById("npv").style.display = "none";
      document.getElementById("c-npv").style.display = "none";

      document.getElementById("dragdrop").className =
        "nav-link active nav-link-custom";
      document.getElementById("elect-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("cool-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("net-pv").className = "nav-link  nav-link-custom";
      document.getElementById("c-net-pv").className =
        "nav-link  nav-link-custom";
    }

    document.getElementById("elect-load").addEventListener("click", showEl);

    function showEl() {
      document.getElementById("drag").style.display = "none";
      document.getElementById("e-load").style.display = "block";
      document.getElementById("c-load").style.display = "none";
      document.getElementById("npv").style.display = "none";
      document.getElementById("c-npv").style.display = "none";

      document.getElementById("elect-load").className =
        "nav-link active nav-link-custom";
      document.getElementById("cool-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("dragdrop").className =
        "nav-link  nav-link-custom";
      document.getElementById("net-pv").className = "nav-link  nav-link-custom";
      document.getElementById("c-net-pv").className =
        "nav-link  nav-link-custom";
    }
    document.getElementById("cool-load").addEventListener("click", showCool);

    function showCool() {
      document.getElementById("drag").style.display = "none";
      document.getElementById("e-load").style.display = "none";
      document.getElementById("c-load").style.display = "block";
      document.getElementById("npv").style.display = "none";
      document.getElementById("c-npv").style.display = "none";

      document.getElementById("cool-load").className =
        "nav-link active nav-link-custom";
      document.getElementById("dragdrop").className =
        "nav-link  nav-link-custom";
      document.getElementById("elect-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("net-pv").className = "nav-link  nav-link-custom";
      document.getElementById("c-net-pv").className =
        "nav-link  nav-link-custom";
    }
    document.getElementById("net-pv").addEventListener("click", showNpv);

    function showNpv() {
      document.getElementById("drag").style.display = "none";
      document.getElementById("e-load").style.display = "none";
      document.getElementById("c-load").style.display = "none";
      document.getElementById("npv").style.display = "block";
      document.getElementById("c-npv").style.display = "none";

      document.getElementById("cool-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("dragdrop").className =
        "nav-link  nav-link-custom";
      document.getElementById("elect-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("net-pv").className =
        "nav-link active nav-link-custom";
      document.getElementById("c-net-pv").className =
        "nav-link  nav-link-custom";
    }

    document.getElementById("c-net-pv").addEventListener("click", showCNpv);

    function showCNpv() {
      document.getElementById("drag").style.display = "none";
      document.getElementById("e-load").style.display = "none";
      document.getElementById("c-load").style.display = "none";
      document.getElementById("npv").style.display = "none";
      document.getElementById("c-npv").style.display = "block";

      document.getElementById("c-net-pv").style.display = "block";
      document.getElementById("cool-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("dragdrop").className =
        "nav-link  nav-link-custom";
      document.getElementById("elect-load").className =
        "nav-link  nav-link-custom";
      document.getElementById("net-pv").className = "nav-link  nav-link-custom";
      document.getElementById("c-net-pv").className =
        "nav-link active nav-link-custom";
    }

    Highcharts.chart("electric", this.options);
    Highcharts.chart("cooling", this.options2);
    Highcharts.chart("net", this.options3);
    Highcharts.chart("cnpv", this.options4);

    document.getElementById("homeSel").style.fill = "white";
    document.getElementById("masterSel").style.fill = "#196eed";
    document.getElementById("schedSel").style.fill = "white";

    document.getElementById("inp-sel").style.color = "white";
    document.getElementById("opt-sel").style.color = "white";
    document.getElementById("build-sel").style.color = "white";
    document.getElementById("opt-res").style.color = "#196eed";
  }

  // ------------- PER I GRAFICI-------------------------

  public cardColor = "#212B46";

  public options: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: "areaspline",
      zoomType: "xy"
    },

    plotOptions: {
      areaspline: {
        marker: {
          enabled: false
        }
      }
    },

    legend: {
      layout: "horizontal",
      align: "center",
      verticalAlign: "bottom",
      itemStyle: {
        color: "white",
        fontWeight: "bold"
      }
    },

    title: {
      style: {
        color: "white"
      },
      text: ""
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: "time [hr]"
      },

      categories: [
        "0.5",
        "1",
        "1.5",
        "2",
        "2.5",
        "3",
        "3.5",
        "4",
        "4.5",
        "5",
        "5.5",
        "6",
        "6.5",
        "7",
        "7.5",
        "8",
        "8.5",
        "9",
        "9.5",
        "10",
        "10.5",
        "11",
        "11.5",
        "12",
        "12.5",
        "13",
        "13.5",
        "14",
        "14.5",
        "15",
        "15.5",
        "16",
        "16.5",
        "17",
        "17.5",
        "18",
        "18.5",
        "19",
        "19.5",
        "20",
        "20.5",
        "21",
        "21.5",
        "22",
        "22.5",
        "23",
        "23.5",
        "24"
      ]
    },

    yAxis: [
      {
        //--- Primary yAxis
        gridLineWidth: 0.2,
        lineWidth: 1,
        title: {
          text: "Energy [kW]"
        }
      },
      {
        //--- Secondary yAxis
        lineWidth: 1,
        title: {
          text: "Price [$/kWh]"
        },
        opposite: true
      }
    ],

    exporting: {
      enabled: false
    },

    credits: {
      enabled: false
    },

    series: [
      {
        lineWidth: 3.5,
        color: "white",
        // color: {
        //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        //   stops: [
        //       [0, 'yellow'],
        //       [1, 'transparent']
        //         ],
        // },

        name: "Pload",
        yAxis: 0,
        type: "line",
        marker: {
          enabled: false
        },
        data: [
          1442.76,
          1414.72,
          1432.12,
          1390.06,
          1322.19,
          1317.6,
          1296.74,
          1383.16,
          1357.54,
          1356.34,
          1404,
          1364.33,
          1319.99,
          1448.06,
          1321.26,
          1412.97,
          1896,
          2090.1,
          1907.48,
          1906.95,
          1964.91,
          2043.27,
          2109.78,
          2023.31,
          2063.6,
          2064.18,
          1965.29,
          2132.16,
          2105.23,
          2051.25,
          2046.14,
          2306.24,
          2031.07,
          2235.85,
          2032.08,
          2200.25,
          2188.76,
          1855.76,
          1746.16,
          1488.17,
          1753.84,
          1437.64,
          1409.66,
          1364.16,
          1399.97,
          1339.62,
          1414.79,
          1513.98
        ]
      },
      {
        lineWidth: 1.5,
        color: "yellow",
        type: "line",
        marker: {
          enabled: false
        },
        name: "PGimp",

        data: [
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          344.61,
          519.02,
          336.75,
          112.96,
          290.72,
          606.63,
          30.62,
          164.93,
          302.36,
          343.2,
          292.9,
          0,
          0
        ]
      },

      {
        lineWidth: 1.5,
        color: "#FF4500",
        // fillColor: '#FF4500',

        type: "line",
        marker: {
          enabled: false
        },
        name: "Ppv",

        data: [
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          7.54,
          30.17,
          116.77,
          358.77,
          469.87,
          469.07,
          469.07,
          469.07,
          147.62,
          290.41,
          467.47,
          352.58,
          352.58,
          352.58,
          284.95,
          222.33,
          220.87,
          220.87,
          220.87,
          141.9,
          30.17,
          7.54,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0
        ]
      },
      {
        lineWidth: 1.5,
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, "#DC143C"],
            [1, "transparent"]
          ]
        },
        // fillColor: '#FF4500',

        type: "area",
        marker: {
          enabled: false
        },
        name: "PChp1",

        data: [
          1442.77,
          1414.71,
          1432.13,
          1390.06,
          1322.19,
          1317.61,
          1296.74,
          1348.18,
          1225,
          1356.34,
          1404.01,
          1297.94,
          1312.45,
          1417.9,
          1204.5,
          1054.2,
          1426.13,
          1621.02,
          1411.21,
          1437.87,
          1517.29,
          1752.86,
          1642.32,
          1670.73,
          1711.03,
          1711.6,
          1680.34,
          1909.84,
          1884.36,
          1830.38,
          1825.27,
          2000,
          2000,
          2000,
          2000,
          1855.64,
          1669.74,
          1519.01,
          1348.18,
          1197.45,
          1147.21,
          1107.02,
          1076.87,
          1061.8,
          1056.77,
          1046.73,
          1414.8,
          1513.98
        ]
      },

      // {
      //   lineWidth:1.5,

      // color: {
      //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
      //   stops: [
      //       [0, '#00e500'],
      //       [1, 'transparent']
      //         ],
      // },

      // // fillColor: '#FF4500',

      // type: 'area',
      // marker: {

      //   enabled: false
      // },

      //   name: 'Pach1',

      //   data:[
      //     151.49,
      //     148.55,
      //     150.37,
      //     145.96,
      //     138.83,
      //     138.35,
      //     136.16,
      //     141.56,
      //     128.62,
      //     142.42,
      //     147.42,
      //     136.28,
      //     137.81,
      //     148.88,
      //     126.47,
      //     110.69,
      //     149.74,
      //     170.21,
      //     148.18,
      //     150.98,
      //     159.32,
      //     184.05,
      //     172.44,
      //     175.43,
      //     179.66,
      //     179.72,
      //     176.44,
      //     200.53,
      //     197.86,
      //     192.19,
      //     191.65,
      //     210,
      //     210,
      //     210,
      //     210,
      //     194.84,
      //     175.32,
      //     159.5,
      //     141.56,
      //     125.73,
      //     120.46,
      //     116.24,
      //     113.07,
      //     111.49,
      //     110.96,
      //     109.91,
      //     148.55,
      //     158.97

      //     ]
      //     },

      // {
      //   lineWidth:1.5,

      // color: {
      //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
      //   stops: [
      //       [0, '#8B0000'],
      //       [1, 'transparent']
      //         ],
      // },

      // // fillColor: '#FF4500',

      // type: 'area',
      // marker: {

      //   enabled: false
      // },

      //   name: 'Pvch1',

      //   data:[
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     160.14,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     203.23,
      //     218.31,
      //     551.96,
      //     412.1,
      //     284.59,
      //     375.57,
      //     284.14,
      //     284.59,
      //     284.59,
      //     282.47,
      //     268.49,
      //     267.26,
      //     273.21,
      //     241.99,
      //     261.42,
      //     262.43,
      //     279.89,
      //     247.25,
      //     233.87,
      //     219.33,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06,
      //     159.06

      //     ]
      //     },

      {
        lineWidth: 1.5,
        color: "yellowgreen",

        type: "histogram",
        marker: {
          enabled: false
        },
        name: "Ebat1",
        data: [
          150,
          150,
          150,
          150,
          150,
          150,
          150,
          132.06,
          64.06,
          64.06,
          64.06,
          30,
          30,
          30,
          30,
          30,
          123.79,
          270,
          256.05,
          263.73,
          109.84,
          219.52,
          270,
          270,
          270,
          270,
          270,
          270,
          270,
          270,
          270,
          185.7,
          185.23,
          68.11,
          51.65,
          123.8,
          270,
          270,
          123.8,
          123.8,
          270,
          116.11,
          30,
          30,
          30,
          30,
          66.55,
          150
        ]
      },

      {
        lineWidth: 1.5,

        color: "red",

        // fillColor: '#FF4500',

        type: "line",
        marker: {
          enabled: false
        },
        yAxis: 1,
        opposite: true,
        name: "pGr [$/kW]",
        dashStyle: "shortdot",

        data: [
          0.1522,
          0.1454,
          0.1398,
          0.1308,
          0.1288,
          0.1284,
          0.1284,
          0.1284,
          0.1274,
          0.1284,
          0.1296,
          0.1468,
          0.1542,
          0.1626,
          0.142,
          0.156,
          0.161,
          0.1628,
          0.1914,
          0.2134,
          0.2764,
          0.3004,
          0.2894,
          0.2804,
          0.2784,
          0.2804,
          0.3006,
          0.3242,
          0.3242,
          0.3004,
          0.3004,
          0.3004,
          0.3004,
          0.2864,
          0.2114,
          0.1724,
          0.1668,
          0.173,
          0.1914,
          0.1914,
          0.1772,
          0.2116,
          0.197,
          0.1914,
          0.163,
          0.1594,
          0.152,
          0.1482
        ]
      }
    ]
  };

  public options2: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: "areaspline",
      zoomType: "xy"
    },

    plotOptions: {
      areaspline: {
        marker: {
          enabled: false
        }
      }
    },
    legend: {
      layout: "horizontal",
      align: "center",
      verticalAlign: "bottom",
      itemStyle: {
        color: "white",
        fontWeight: "bold"
      }
    },

    title: {
      style: {
        color: "white"
      },
      text: ""
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: "time [hr]"
      },

      categories: [
        "0.5",
        "1",
        "1.5",
        "2",
        "2.5",
        "3",
        "3.5",
        "4",
        "4.5",
        "5",
        "5.5",
        "6",
        "6.5",
        "7",
        "7.5",
        "8",
        "8.5",
        "9",
        "9.5",
        "10",
        "10.5",
        "11",
        "11.5",
        "12",
        "12.5",
        "13",
        "13.5",
        "14",
        "14.5",
        "15",
        "15.5",
        "16",
        "16.5",
        "17",
        "17.5",
        "18",
        "18.5",
        "19",
        "19.5",
        "20",
        "20.5",
        "21",
        "21.5",
        "22",
        "22.5",
        "23",
        "23.5",
        "24"
      ]
    },

    yAxis: [
      {
        //--- Primary yAxis
        gridLineWidth: 0.2,
        lineWidth: 1,
        title: {
          text: "Energy [kW]"
        }
      },
      {
        //--- Secondary yAxis
        lineWidth: 1,
        title: {
          text: "Price [$/kWh]"
        },
        opposite: true
      }
    ],

    exporting: {
      enabled: false
    },

    credits: {
      enabled: false
    },

    series: [
      {
        lineWidth: 3.5,
        color: "#196eed",
        // color: {
        //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        //   stops: [
        //       [0, 'yellow'],
        //       [1, 'transparent']
        //         ],
        // },

        name: "Cload",
        yAxis: 0,
        type: "line",
        marker: {
          enabled: false
        },
        data: [
          2042.46,
          2013,
          2031.29,
          1987.11,
          1925.56,
          1911.03,
          1889.13,
          1943.14,
          1813.8,
          1951.71,
          2001.75,
          1890.39,
          1905.62,
          2016.34,
          2368.7,
          2347.6,
          4310.08,
          4141.27,
          3323.57,
          3803.64,
          3254.98,
          3682.3,
          3566.24,
          3576.79,
          3492.38,
          3481.83,
          3460.73,
          3460.73,
          3434.35,
          3386.87,
          3539.86,
          3603.17,
          3481.83,
          3349.94,
          2702.81,
          2475.97,
          2280.77,
          2122.51,
          1943.14,
          1784.88,
          1732.12,
          1689.92,
          1658.27,
          1642.44,
          1637.16,
          1626.61,
          2013.09,
          2117.23
        ]
      },
      {
        lineWidth: 1.5,
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, "green"],
            [1, "transparent"]
          ]
        },
        type: "area",
        marker: {
          enabled: false
        },
        name: "Qachl1",

        data: [
          1514.91,
          1485.45,
          1503.73,
          1459.56,
          1388.3,
          1383.49,
          1361.58,
          1415.59,
          1286.24,
          1424.16,
          1474.21,
          1362.84,
          1378.07,
          1488.79,
          1264.73,
          1106.91,
          1497.44,
          1702.08,
          1481.77,
          1509.76,
          1593.15,
          1840.5,
          1724.44,
          1754.27,
          1796.58,
          1797.18,
          1764.35,
          2005.33,
          1978.57,
          1921.9,
          1916.54,
          2100,
          2100,
          2100,
          2100,
          1948.42,
          1753.22,
          1594.96,
          1415.59,
          1257.33,
          1204.57,
          1162.37,
          1130.72,
          1114.89,
          1109.61,
          1099.06,
          1485.54,
          1589.68
        ]
      },

      {
        lineWidth: 1.5,
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
            [0, "#00FA9A"],
            [1, "transparent"]
          ]
        },
        // fillColor: '#FF4500',

        type: "area",
        marker: {
          enabled: false
        },
        name: "Qvch1",

        data: [
          527.55,
          527.55,
          527.55,
          527.55,
          537.26,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          928.12,
          1064.8,
          2636.8,
          2263.3,
          1665.9,
          2118.0,
          1661.8,
          1665.9,
          1665.9,
          1646.6,
          1519.9,
          1508.8,
          1562.6,
          1279.5,
          1455.7,
          1464.9,
          1623.3,
          1327.3,
          1205.9,
          1074.0,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55,
          527.55
        ]
      },

      {
        lineWidth: 1.5,
        color: "#00FFFF",
        opacity: 0.6,

        type: "histogram",
        marker: {
          enabled: false
        },
        name: "ETES [kWh]",

        data: [
          879.25,
          947.47,
          1011.6,
          1077.3,
          1077.3,
          1085.0,
          1158.8,
          1238.0,
          1239.9,
          1303.9,
          1378.5,
          1457.7,
          1477.2,
          1546.5,
          1454.0,
          1361.4,
          1268.9,
          1176.3,
          1083.8,
          991.26,
          991.26,
          898.7,
          806.15,
          713.6,
          621.05,
          528.49,
          458.14,
          365.58,
          365.58,
          365.58,
          365.58,
          273.03,
          180.48,
          87.92,
          127.44,
          206.58,
          285.71,
          364.84,
          443.97,
          523.11,
          602.24,
          681.37,
          760.5,
          839.64,
          918.77,
          997.9,
          1023.0,
          1102.18
        ]
      },

      {
        yAxis: 1,
        lineWidth: 1.5,
        dashStyle: "shortdot",
        color: "red",
        type: "line",
        marker: {
          enabled: false
        },
        opposite: true,
        name: "pGr",

        data: [
          0.1522,
          0.1454,
          0.1398,
          0.1308,
          0.1288,
          0.1284,
          0.1284,
          0.1284,
          0.1274,
          0.1284,
          0.1296,
          0.1468,
          0.1542,
          0.1626,
          0.142,
          0.156,
          0.161,
          0.1628,
          0.1914,
          0.2134,
          0.2764,
          0.3004,
          0.2894,
          0.2804,
          0.2784,
          0.2804,
          0.3006,
          0.3242,
          0.3242,
          0.3004,
          0.3004,
          0.3004,
          0.3004,
          0.2864,
          0.2114,
          0.1724,
          0.1668,
          0.173,
          0.1914,
          0.1914,
          0.1772,
          0.2116,
          0.197,
          0.1914,
          0.163,
          0.1594,
          0.152,
          0.1482
        ]
      }
    ]
  };

  public options3: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: "column",
      zoomType: "xy"
    },

    legend: {
      layout: "horizontal",
      align: "center",
      verticalAlign: "bottom",
      itemStyle: {
        color: "white",
        fontWeight: "bold"
      }
    },

    title: {
      style: {
        color: "white"
      },
      text: ""
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: "time [yrs]"
      },

      categories: [
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20"
      ]
    },

    yAxis: [
      {
        //--- Primary yAxis
        gridLineWidth: 0.2,
        title: {
          text: "NPV [$]"
        }
      }
    ],

    exporting: {
      enabled: false
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [
      {
        color: "green",
        name: "npv",
        data: [
          -4271000,
          -2574000,
          -973000,
          537000,
          1962000,
          3307000,
          4575000,
          5771000,
          6900000,
          7965000,
          8969000,
          9917000,
          10811000,
          11654000,
          12450000,
          13200000,
          13908000,
          14576000,
          15207000,
          15801000,
          16362000
        ]
      }
    ]
  };

  public options4: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: "column",
      zoomType: "xy"
    },

    legend: {
      layout: "horizontal",
      align: "center",
      verticalAlign: "bottom",
      itemStyle: {
        color: "white",
        fontWeight: "bold"
      }
    },

    title: {
      style: {
        color: "white"
      },
      text: ""
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: "time [yrs]"
      },

      categories: [
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20"
      ]
    },

    yAxis: [
      {
        //--- Primary yAxis
        gridLineWidth: 0.2,
        title: {
          text: "NPV [$]"
        }
      }
    ],

    exporting: {
      enabled: false
    },

    credits: {
      enabled: false
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [
      {
        name: "Case 1",
        data: [
          -2523000,
          -1992700,
          -1492400,
          -1020400,
          -575200,
          -155100,
          241200,
          615000,
          967700,
          1300400,
          1614300,
          1910400,
          2189800,
          2453300,
          2702000,
          2936500,
          3157800,
          3366500,
          3563500,
          3749300,
          3924500
        ]
      },
      {
        name: "Case 2",
        color: "red",
        data: [
          -2929000,
          -1863000,
          -857000,
          92000,
          987000,
          1831000,
          2627000,
          3379000,
          4088000,
          4757000,
          5387000,
          5983000,
          6544000,
          7074000,
          7574000,
          8045000,
          8490000,
          8909000,
          9305000,
          9679000,
          10031000
        ]
      },
      {
        name: "Case 3",
        color: "yellow",
        data: [
          -4590500,
          -3698200,
          -2856400,
          -2062300,
          -1313100,
          -606400,
          60400,
          689400,
          1282800,
          1842600,
          2370800,
          2869000,
          3339100,
          3782500,
          4200800,
          4595500,
          4967800,
          5319000,
          5650400,
          5963000,
          6257900
        ]
      },
      {
        name: "Case 4",
        color: "green",
        data: [
          -4271000,
          -2574000,
          -973000,
          537000,
          1962000,
          3307000,
          4575000,
          5771000,
          6900000,
          7965000,
          8969000,
          9917000,
          10811000,
          11654000,
          12450000,
          13200000,
          13908000,
          14576000,
          15207000,
          15801000,
          16362000
        ]
      }
    ]
  };
}
