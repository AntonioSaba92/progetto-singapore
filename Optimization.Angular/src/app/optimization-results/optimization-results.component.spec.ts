import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptimizationResultsComponent } from './optimization-results.component';

describe('OptimizationResultsComponent', () => {
  let component: OptimizationResultsComponent;
  let fixture: ComponentFixture<OptimizationResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptimizationResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimizationResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
