import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dragdrop2Component } from './dragdrop2.component';

describe('Dragdrop2Component', () => {
  let component: Dragdrop2Component;
  let fixture: ComponentFixture<Dragdrop2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dragdrop2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dragdrop2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
