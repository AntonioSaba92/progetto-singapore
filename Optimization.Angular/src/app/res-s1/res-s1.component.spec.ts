import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResS1Component } from './res-s1.component';

describe('ResS1Component', () => {
  let component: ResS1Component;
  let fixture: ComponentFixture<ResS1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResS1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
