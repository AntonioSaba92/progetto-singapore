
import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'app-res-s1',
  templateUrl: './res-s1.component.html',
  styleUrls: ['./res-s1.component.scss']
})
export class ResS1Component implements OnInit {

  constructor() { }

  ngOnInit() {

    document.getElementById('dragdrop').addEventListener('click',showDrag);

    function showDrag(){
      document.getElementById('drag').style.display = "block";
      document.getElementById('e-load').style.display = "none";
      document.getElementById('c-load').style.display = "none";
      document.getElementById('npv').style.display = "none";
      document.getElementById('c-npv').style.display = "none";

      document.getElementById('dragdrop').className = "nav-link active nav-link-custom";
      document.getElementById('elect-load').className = "nav-link  nav-link-custom";
      document.getElementById('cool-load').className = "nav-link  nav-link-custom";
      document.getElementById('net-pv').className = "nav-link  nav-link-custom";
      document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";

      
      


    }

    document.getElementById('elect-load').addEventListener('click',showEl);

    function showEl(){
      document.getElementById('drag').style.display = "none";
      document.getElementById('e-load').style.display = "block";
      document.getElementById('c-load').style.display = "none";
      document.getElementById('npv').style.display = "none";
      document.getElementById('c-npv').style.display = "none";

      document.getElementById('elect-load').className = "nav-link active nav-link-custom";
      document.getElementById('cool-load').className = "nav-link  nav-link-custom";
      document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
      document.getElementById('net-pv').className = "nav-link  nav-link-custom";
      document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";





      
    }
    document.getElementById('cool-load').addEventListener('click',showCool);

    function showCool(){
      document.getElementById('drag').style.display = "none";
      document.getElementById('e-load').style.display = "none";
      document.getElementById('c-load').style.display = "block";
      document.getElementById('npv').style.display = "none";
      document.getElementById('c-npv').style.display = "none";

       document.getElementById('cool-load').className = "nav-link active nav-link-custom";
      document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
      document.getElementById('elect-load').className = "nav-link  nav-link-custom";
      document.getElementById('net-pv').className = "nav-link  nav-link-custom";
      document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";



    }
    document.getElementById('net-pv').addEventListener('click',showNpv);


function showNpv(){
  document.getElementById('drag').style.display = "none";
  document.getElementById('e-load').style.display = "none";
  document.getElementById('c-load').style.display = "none";
  document.getElementById('npv').style.display = "block";
  document.getElementById('c-npv').style.display = "none";

   document.getElementById('cool-load').className = "nav-link  nav-link-custom";
  document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
  document.getElementById('elect-load').className = "nav-link  nav-link-custom";
  document.getElementById('net-pv').className = "nav-link active nav-link-custom";
  document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";

}

document.getElementById('c-net-pv').addEventListener('click',showCNpv);


function showCNpv(){
  document.getElementById('drag').style.display = "none";
  document.getElementById('e-load').style.display = "none";
  document.getElementById('c-load').style.display = "none";
  document.getElementById('npv').style.display = "none";
  document.getElementById('c-npv').style.display = "block";

  document.getElementById('c-net-pv').style.display = "block";
   document.getElementById('cool-load').className = "nav-link  nav-link-custom";
  document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
  document.getElementById('elect-load').className = "nav-link  nav-link-custom";
  document.getElementById('net-pv').className = "nav-link  nav-link-custom";
  document.getElementById('c-net-pv').className = "nav-link active nav-link-custom";

}




Highcharts.chart('electric', this.options);
Highcharts.chart('cooling', this.options2);
Highcharts.chart('net', this.options3);
Highcharts.chart('cnpv', this.options4);



    document.getElementById('homeSel').style.fill = "white";
    document.getElementById('masterSel').style.fill = "#196eed";
    document.getElementById('schedSel').style.fill = "white";

    document.getElementById('inp-sel').style.color = "white"
    document.getElementById('opt-sel').style.color = "white";
    document.getElementById('build-sel').style.color = "white";
    document.getElementById('opt-res').style.color = "#196eed";

  }


// ------------- PER I GRAFICI-------------------------




public cardColor = '#212B46';

public options: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'areaspline',
     zoomType: 'xy',
},

plotOptions: {
  areaspline: {
      marker: {
          enabled: false
      }
  }
},

legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [hr]',
  },
  


  categories: [
    '0.5',
    '1',
    '1.5',
    '2',
    '2.5',
    '3',
    '3.5',
    '4',
    '4.5',
    '5',
    '5.5',
    '6',
    '6.5',
    '7',
    '7.5',
    '8',
    '8.5',
    '9',
   '9.5',
    '10',
    '10.5',
    '11',
    '11.5',
    '12',
    '12.5',
    '13',
    '13.5',
    '14',
    '14.5',
    '15',
    '15.5',
    '16',
    '16.5',
    '17',
    '17.5',
    '18',
    '18.5',
    '19',
    '19.5',
    '20',
    '20.5',
    '21',
    '21.5',
    '22',
    '22.5',
    '23',
    '23.5',
    '24',
    
  ]
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
  lineWidth: 1,
    title: {
        text: 'Energy [kW]'
    },

},
{ //--- Secondary yAxis
  lineWidth: 1,
        title: {
            text: 'Price [$/kWh]',
         rotation: 270
        },
        opposite: true,
    }

],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},

  series: [{
lineWidth:3.5,
color: 'white',
// color: {
//   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
//   stops: [
//       [0, 'yellow'],
//       [1, 'transparent']
//         ],
// },

name: 'Pload',
yAxis: 0,
type: 'line',
marker: {
  enabled: false
},
data: [
  1002.59,
  988.88,
  994.86,
  920.43,
  908.29,
  903.62,
  877.08,
  934.61,
  929.69,
  919.69,
  943.15,
  948,
  924.83,
  1004.95,
  924.77,
  976.5,
  1090.84,
  1209.38,
  1321.02,
  1259.99,
  1307.05,
  1263,
  1388.95,
  1356.11,
  1398.72,
  1382.71,
  1313.24,
  1429.24,
  1397.91,
  1361.07,
  1359.15,
  1547.37,
  1362.71,
  1497.91,
  1376.14,
  1389.74,
  1281.98,
  1270.53,
  1202.54,
  1030.55,
  1007.67,
  996.86,
  978.21,
  947.88,
  971.76,
  931.52,
  926.1,
  920.52,
  
  ]
},
{
  lineWidth:1.5,
color: 'yellow',
type: 'line',
marker: {
  enabled: false
},
  name: 'PGimp',
 
  data:[
    2.59,
    0,
    0,
    920.43,
    908.29,
    903.62,
    877.08,
    934.61,
    929.69,
    919.69,
    943.15,
    46.97,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    110.51,
    0,
    0,
    25.53,
    68.13,
    52.12,
    26.89,
    183.84,
    153.46,
    116.62,
    114.7,
    354.56,
    242.98,
    392.98,
    276.14,
    289.74,
    281.98,
    270.53,
    268.02,
    196.52,
    207.13,
    223.12,
    224.56,
    204.28,
    231.51,
    197.97,
    0,
    0
    
]
},

{
  lineWidth:1.5,
color: '#FF4500',
// fillColor: '#FF4500',

type: 'line',
marker: {

  enabled: false
},
  name: 'Ppv',
 
  data:[

    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    4.93,
    19.73,
    76.36,
    234.64,
    307.3,
    306.77,
    306.77,
    306.77,
    96.54,
    189.93,
    305.73,
    230.59,
    230.59,
    230.59,
    186.36,
    145.4,
    144.45,
    144.45,
    144.45,
    92.8,
    19.73,
    4.93,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
    
        
    ]
    },
    {
      lineWidth:1.5,
      color: {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        stops: [
            [0, '#DC143C'],
            [1, 'transparent']
              ],
      },
    // fillColor: '#FF4500',
    
    type: 'area',
    marker: {

      enabled: false
    },
      name: 'PChp1',
     
      data:[
        1000,
        988.89,
        994.86,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        901.02,
        919.89,
        985.22,
        848.41,
        741.86,
        700.42,
        865.64,
        977.28,
        916.24,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        934.52,
        834.03,
        800.54,
        773.74,
        753.64,
        743.59,
        740.24,
        733.55,
        926.1,
        920.52
        
   
        ]
        },
        {
          lineWidth:1.5,
          color: {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
                [0, '#B22222'],
                [1, 'transparent']
                  ],
          },

        
        type: 'area',
        marker: {
    
          enabled: false
        },
          name: 'PChp2',
         
          data:[
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            83.12,
            36.97,
            36.97,
            36.97,
            100,
            73.07,
            83.23,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
            
            
       
            ]
            },
            // {
            //   lineWidth:1.5,
    
            // color: {
            //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            //   stops: [
            //       [0, '#00e500'],
            //       [1, 'transparent']
            //         ],
            // },
  
            // // fillColor: '#FF4500',
            
            // type: 'area',
            // marker: {
            
            //   enabled: false
            // },

            //   name: 'Pach1',
             
            //   data:[
            //     105,
            //     103.83,
            //     104.46,
            //     0,
            //     0,
            //     0,
            //     0,
            //     0,
            //     0,
            //     0,
            //     0,
            //     94.61,
            //     96.59,
            //     103.45,
            //     89.08,
            //     77.9,
            //     73.54,
            //     90.89,
            //     102.61,
            //     96.21,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     98.12,
            //     87.57,
            //     84.06,
            //     81.24,
            //     79.13,
            //     78.08,
            //     77.73,
            //     77.02,
            //     97.24,
            //     96.65,
                
                
                    
            //     ]
            //     },
            //     {
            //       lineWidth:1.5,
        
            //     color: {
            //       linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            //       stops: [
            //           [0, '#006600'],
            //           [1, 'transparent']
            //             ],
            //     },
      
            //     // fillColor: '#FF4500',
                
            //     type: 'area',
            //     marker: {
                
            //       enabled: false
            //     },
    
            //       name: 'Pach2',
                 
            //       data:[
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         9.81,
            //         4.36,
            //         4.36,
            //         4.36,
            //         11.8,
            //         8.62,
            //         9.82,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0
                    
                        
            //         ]
            //         },
            // {
            //   lineWidth:1.5,
    
            // color: {
            //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            //   stops: [
            //       [0, '#8B0000'],
            //       [1, 'transparent']
            //         ],
            // },
  
            // // fillColor: '#FF4500',
            
            // type: 'area',
            // marker: {
            
            //   enabled: false
            // },

            //   name: 'Pvch1',
             
            //   data:[
            //     142.78,
            //     146.98,
            //     141.94,
            //     197.07,
            //     226.14,
            //     223.49,
            //     209.4,
            //     212.92,
            //     216.45,
            //     216.45,
            //     211.47,
            //     140.73,
            //     146.16,
            //     141.42,
            //     174.65,
            //     175.95,
            //     339.6,
            //     308.94,
            //     230.91,
            //     249.67,
            //     175.95,
            //     249.67,
            //     241.34,
            //     195.7,
            //     204.95,
            //     187.78,
            //     186.01,
            //     186.01,
            //     183.81,
            //     179.85,
            //     192.62,
            //     197.91,
            //     187.78,
            //     176.76,
            //     150.66,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73
                
            //     ]
            //     },
                {
                  lineWidth:1.5,
                  color: '00FFFF',
        
                
                type: 'histogram',
                marker: {
            
                  enabled: false
                },
                  name: 'Ebat1',
                 
                  data:[
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                    
                    
               
                    ]
                    },

                    {
                      lineWidth:1.5,
            
                    color: 'red',
          
                    // fillColor: '#FF4500',
                    
                    type: 'line',
                    marker: {
                    
                      enabled: false
                    },
                    yAxis:1,
                    opposite: true,
                      name: 'pGr [$/kW]',
                      dashStyle: 'shortdot',
                     
data:[
  0.1103,
  0.1054,
  0.1014,
  0.0948,
  0.0934,
  0.0931,
  0.0931,
  0.0931,
  0.0924,
  0.0931,
  0.094,
  0.1064,
  0.1118,
  0.1179,
  0.1029,
  0.1131,
  0.1167,
  0.118,
  0.1388,
  0.1547,
  0.2004,
  0.2178,
  0.2098,
  0.2033,
  0.2018,
  0.2033,
  0.2179,
  0.235,
  0.235,
  0.2178,
  0.2178,
  0.2178,
  0.2178,
  0.2076,
  0.1533,
  0.125,
  0.1209,
  0.1254,
  0.1388,
  0.1388,
  0.1285,
  0.1534,
  0.1428,
  0.1388,
  0.1182,
  0.1156,
  0.1102,
  0.1074
  

                            
]
},  
]
}

public options2: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'areaspline',
     zoomType: 'xy',
},

plotOptions: {
  areaspline: {
      marker: {
          enabled: false
      }
  }
},
legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [hr]',
  },
  


  categories: [
    '0.5',
    '1',
    '1.5',
    '2',
    '2.5',
    '3',
    '3.5',
    '4',
    '4.5',
    '5',
    '5.5',
    '6',
    '6.5',
    '7',
    '7.5',
    '8',
    '8.5',
    '9',
   '9.5',
    '10',
    '10.5',
    '11',
    '11.5',
    '12',
    '12.5',
    '13',
    '13.5',
    '14',
    '14.5',
    '15',
    '15.5',
    '16',
    '16.5',
    '17',
    '17.5',
    '18',
    '18.5',
    '19',
    '19.5',
    '20',
    '20.5',
    '21',
    '21.5',
    '22',
    '22.5',
    '23',
    '23.5',
    '24',
    
  ]
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
  lineWidth: 1,
    title: {
        text: 'Energy [kW]'
    },

},
{ //--- Secondary yAxis
  lineWidth: 1,
        title: {
            text: 'Price [$/kWh]',
         rotation: 270

        },
        opposite: true
    }

],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},

  series: [{
lineWidth:3.5,
color: '#196eed',
// color: {
//   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
//   stops: [
//       [0, 'yellow'],
//       [1, 'transparent']
//         ],
// },

name: 'Cload',
yAxis: 0,
type: 'line',
marker: {
  enabled: false
},
data: [
  1452.52,
  1501.76,
  1434.94,
  1227.43,
  1283.7,
  1262.6,
  1150.06,
  1178.19,
  1206.33,
  1206.33,
  1223.92,
  1318.87,
  1417.35,
  1417.35,
  1754.98,
  1661.91,
  2873.39,
  2760.84,
  2391.56,
  2535.76,
  2169.99,
  2607.81,
  2553.34,
  2384.53,
  2328.25,
  2321.22,
  2307.15,
  2307.15,
  2289.57,
  2257.91,
  2359.91,
  2402.11,
  2321.22,
  2233.3,
  1684.64,
  1584.38,
  1422.8,
  1473.62,
  1354.04,
  1248.53,
  1213.36,
  1185.23,
  1164.13,
  1153.58,
  1150.06,
  1143.02,
  1345.21,
  1339.35
  
  

  ]
},
{
  lineWidth:1.5,
  color: {
    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
    stops: [
        [0, 'green'],
        [1, 'transparent']
          ],
  },
type: 'area',
marker: {
  enabled: false
},
  name: 'Qachl1',
 
  data:[
    1050,
    1038.33,
    1044.6,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    946.07,
    965.89,
    1034.4,
    890.83,
    778.95,
    735.45,
    908.92,
    1026.1,
    962.05,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    1050,
    981.24,
    875.73,
    840.56,
    812.43,
    791.33,
    780.77,
    777.26,
    770.22,
    972.4,
    966.55
    
    
]
},

{
  lineWidth:1.5,
color: '#ADFF2F',
// fillColor: '#FF4500',

type: 'area',
marker: {

  enabled: false
},
  name: 'Qachl2',
 
  data:[
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    98.08,
    43.62,
    43.62,
    43.62,
    118,
    86.22,
    98.21,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
    
    
        
    ]
    },
    {
      lineWidth:1.5,
      color: {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        stops: [
            [0, '#00FA9A'],
            [1, 'transparent']
              ],
      },
    // fillColor: '#FF4500',
    
    type: 'area',
    marker: {

      enabled: false
    },
      name: 'Qvch1',
     
      data:[
        402.52,
        463.43,
        390.34,
        1051.5,
        1283.7,
        1262.6,
        1150.0,
        1178.1,
        1206.3,
        1206.3,
        1166.6,
        372.8,
        451.46,
        382.87,
        864.15,
        882.95,
        1864.0,
        1730.2,
        1321.7,
        1471.5,
        882.95,
        1471.5,
        1405.1,
        1040.6,
        1114.5,
        977.37,
        963.3,
        963.3,
        945.72,
        914.06,
        1016.0,
        1058.2,
        977.37,
        889.45,
        516.64,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8
        

   
        ]
        },
        
                {
                  lineWidth:1.5,
                  color: '#00FFFF',
                  opacity:0.6,
                
                type: 'histogram',
                marker: {
            
                  enabled: false
                },
                  name: 'ETES [kWh]',
                 
                  data:[
                    527.55,
685.81,
764.95,
764.95,
764.95,
764.95,
764.95,
764.95,
764.95,
764.95,
764.95,
844.08,
923.21,
1002.34,
1002.34,
1002.34,
909.79,
817.24,
896.37,
925.79,
833.24,
856.29,
935.43,
945.09,
852.54,
759.99,
700.62,
608.07,
515.52,
422.97,
330.41,
237.86,
145.31,
52.75,
57.66,
130.63,
209.76,
288.89,
368.02,
447.16,
526.29,
605.42,
684.55,
763.69,
842.82,
921.95,
921.95,
921.95,

                    
               
                    ]
                    },

                    {
                      yAxis:1,
                      lineWidth:1.5,
                      dashStyle: 'shortdot',
                    color: 'red',
                    type: 'line',
                    marker: {
                      enabled: false
                    },
                    opposite:true,
                      name: 'pGr',
                     
                      data:[
                        0.1103,
                        0.1054,
                        0.1014,
                        0.0948,
                        0.0934,
                        0.0931,
                        0.0931,
                        0.0931,
                        0.0924,
                        0.0931,
                        0.094,
                        0.1064,
                        0.1118,
                        0.1179,
                        0.1029,
                        0.1131,
                        0.1167,
                        0.118,
                        0.1388,
                        0.1547,
                        0.2004,
                        0.2178,
                        0.2098,
                        0.2033,
                        0.2018,
                        0.2033,
                        0.2179,
                        0.235,
                        0.235,
                        0.2178,
                        0.2178,
                        0.2178,
                        0.2178,
                        0.2076,
                        0.1533,
                        0.125,
                        0.1209,
                        0.1254,
                        0.1388,
                        0.1388,
                        0.1285,
                        0.1534,
                        0.1428,
                        0.1388,
                        0.1182,
                        0.1156,
                        0.1102,
                        0.1074
                        
                    ]
                    }
                    
  
]
}

public options3: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'column',
     zoomType: 'xy',
},


legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [yrs]',
  },



  categories: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
    title: {
        text: 'NPV [$]'
    },

},


],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},
plotOptions: {
  column: {
    pointPadding: 0.2,
    borderWidth: 0
  }
},
series: [{
  name:'npv',
  data: [-2523000,
    -1992700,
    -1492400,
    -1020400,
    -575200,
    -155100,
     241200,
     615000,
     967700,
     1300400,
     1614300,
     1910400,
     2189800,
     2453300,
     2702000,
     2936500,
     3157800,
     3366500,
     3563500,
     3749300,
     3924500
    ]
  }]

}

public options4: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'column',
     zoomType: 'xy',
},


legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [yrs]',
  },



  categories: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
    title: {
        text: 'NPV [$]'
    },

},


],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},
plotOptions: {
  column: {
    pointPadding: 0.2,
    borderWidth: 0
  }
},
series: [{
  name: 'Case 1',
  data: [-2523000,
    -1992700,
    -1492400,
    -1020400,
    -575200,
    -155100,
     241200,
     615000,
     967700,
     1300400,
     1614300,
     1910400,
     2189800,
     2453300,
     2702000,
     2936500,
     3157800,
     3366500,
     3563500,
     3749300,
     3924500]

}, {
  name: 'Case 2',
  color: 'red',
  data: [-2929000,
    -1863000,
    -857000,
     92000,
     987000, 
     1831000,
     2627000,
     3379000,
     4088000,
     4757000,
     5387000,
     5983000,
     6544000,
     7074000,
     7574000,
     8045000,
     8490000,
     8909000,
     9305000,
     9679000,
     10031000
    ]

}, {
  name: 'Case 3',
  color:'yellow',
  data: [-4590500,
    -3698200,
    -2856400,
    -2062300,
    -1313100,
    -606400,
     60400 ,
     689400,
     1282800,
     1842600,
     2370800,
     2869000,
     3339100,
     3782500,
     4200800,
     4595500,
     4967800,
     5319000,
     5650400,
     5963000,
     6257900,
    ]

}, {
  name: 'Case 4',
  color: 'green',
  data: [-4271000,
    -2574000,
    -973000,
     537000,
     1962000,
     3307000,
     4575000,
     5771000,
     6900000,
     7965000,
     8969000,
     9917000,
     10811000,
     11654000,
     12450000,
     13200000,
     13908000,
     14576000,
     15207000,
     15801000,
     16362000]

}]

}


}




