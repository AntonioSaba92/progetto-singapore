import { Building } from "./building";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor() {}

  // -------------------------PER AGGIUNGERE EDIFICI ALLA TABELLA SUMMMARY-----------------------------------------------------------
  build: Building[];

  private edifici: BehaviorSubject<Building[]> = new BehaviorSubject<
    Building[]
  >(this.build);

  currentBuilding = this.edifici.asObservable();

  changeBuilding(input: Building[]) {
    this.edifici.next(input);
  }
  //------------------------------------------------------------------------------------------------------------------------------------

  //------------------------ PER INSERIRE LE INFO NEL REPORT FINALE

  private messageSource = new BehaviorSubject("default message");
  currentMessage = this.messageSource.asObservable();

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

  //--------------------------------------------------------------------------------------------------------------------------------
}
