import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragdropD2Component } from './dragdrop-d2.component';

describe('DragdropD2Component', () => {
  let component: DragdropD2Component;
  let fixture: ComponentFixture<DragdropD2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragdropD2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragdropD2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
