import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragdropS1Component } from './dragdrop-s1.component';

describe('DragdropS1Component', () => {
  let component: DragdropS1Component;
  let fixture: ComponentFixture<DragdropS1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragdropS1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragdropS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
