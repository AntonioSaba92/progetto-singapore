import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResD2Component } from './res-d2.component';

describe('ResD2Component', () => {
  let component: ResD2Component;
  let fixture: ComponentFixture<ResD2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResD2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResD2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
