import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {



  
  constructor() { }

  ngOnInit() {
    document.getElementById('homeSel').style.fill = "#196eed";
    document.getElementById('masterSel').style.fill = "white";
    document.getElementById('schedSel').style.fill = "white";

    // document.getElementById('summary-tab').style.color = "white"
    document.getElementById('opt-sel').style.color = "white";
    document.getElementById('build-sel').style.color = "white";
    document.getElementById('opt-res').style.color = "white";
    document.getElementById('inp-sel').style.color = "white";
    
  }

}
