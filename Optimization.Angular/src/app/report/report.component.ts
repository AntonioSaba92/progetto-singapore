// import { DataService } from "./../data.service";
import { Component, OnInit } from "@angular/core";
import html2canvas from "html2canvas";
import * as jsPDF from "jspdf";

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.scss"]
})
export class ReportComponent implements OnInit {
  constructor() {}

  // message: string;
  ngOnInit() {
    print();

    //   this.data.currentMessage.subscribe(message => (this.message = message));

    var im = localStorage.getItem("ultimoScreen");
    var elemIm = document.createElement("div");
    elemIm.innerHTML = im;
    console.log(elemIm);

    document.getElementById("titolo").insertAdjacentElement("afterend", elemIm);
  }

  print() {
    const filename = "ThisIsYourPDFFilename.pdf";

    html2canvas(document.querySelector("#main")).then(canvas => {
      let pdf = new jsPDF("p", "mm", "a4");
      // pdf.addImage(canvas.toDataURL("image/png"), "PNG", 0, 0, 211, 298);
      pdf.save(filename);
    });
  }

  // Variant
  // This one lets you improve the PDF sharpness by scaling up the HTML node tree to render as an image before getting pasted on the PDF.
}
