


import { Component, OnInit } from '@angular/core';
import { CdkDragEnter, CdkDragExit } from '@angular/cdk/drag-drop';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-dragdrop-d',
  templateUrl: './dragdrop-d.component.html',
  styleUrls: ['./dragdrop-d.component.scss']
})
export class DragdropDComponent implements OnInit {


  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event.item.data);
   }
   exited(event: CdkDragExit<string[]>) {
     console.log('Exited', event.item.data);
   }

  constructor() { }

  ngOnInit() {
    document.getElementById('homeSel').style.fill = "white";
    document.getElementById('masterSel').style.fill = "#196eed";
    document.getElementById('schedSel').style.fill = "white";

    document.getElementById('inp-sel').style.color = "white"
    document.getElementById('build-sel').style.color = "white";
    document.getElementById('opt-res').style.color = "white";
    document.getElementById('opt-sel').style.color = "#196eed";

    //aggiungo bottone per rimuovere componenti da grafico
      document.getElementById('solar-g').addEventListener('click', removeSol);
      document.getElementById('wind-g').addEventListener('click', removeWind);
      document.getElementById('battery-g').addEventListener('click', removeBattery);
      // document.getElementById('rimchill').addEventListener('click', removeElChiller);
      // document.getElementById('rimcchp').addEventListener('click', removeCchp);
      document.getElementById('cooling-storage-g').addEventListener('click', removeColdStor);
      document.getElementById('chp-g').addEventListener('click', removeChp);
      document.getElementById('heatstor-g').addEventListener('click', removeTermStor);
      document.getElementById('abs-chiller-g').addEventListener('click', removeAbsChiller);
      document.getElementById('vap-chiller-g').addEventListener('click', removeVapChiller);
      document.getElementById('chp-blue-g').addEventListener('click', removeChpBlue);

      function removeChpBlue(){
        document.getElementById('chp-blue-g').style.display="none"; 
        document.getElementById('chp-blue').style.display="block"; 
      }



      // document.getElementById('rimchp').addEventListener('click', removeChp);

      function removeAbsChiller(){
        document.getElementById('abs-chiller-g').style.display="none"; 
        document.getElementById('abs-chiller').style.display="block"; 
        document.getElementById('chp-g').style.display="none"; 
        document.getElementById('chp').style.display="block"; 
      }
      function removeSol(){
        document.getElementById('solar-g').style.display="none"; 
        document.getElementById('solar').style.display="block"; 
      }
      
      function removeWind(){
        document.getElementById('wind-g').style.display="none"; 
        document.getElementById('wind').style.display="block"; 
      }
      function removeBattery(){
        document.getElementById('battery-g').style.display="none"; 
        document.getElementById('battery').style.display="block"; 
      }
      function removeVapChiller(){
        document.getElementById('vap-chiller-g').style.display="none"; 
        document.getElementById('vap-chiller').style.display="block"; 
      }
      // function removeElChiller(){
      //   document.getElementById('elchiller-g').style.display="none"; 
      //   document.getElementById('elchiller').style.display="block"; 
      // }
      // function removeCchp(){
      //   document.getElementById('absorption-g').style.display="none"; 
      //   document.getElementById('absorption').style.display="block"; 
      // }
      function removeColdStor(){
        document.getElementById('cooling-storage-g').style.display="none"; 
        document.getElementById('cooling-storage').style.display="block"; 
      }
      function removeChp(){
        document.getElementById('chp-g').style.display="none"; 
        document.getElementById('chp').style.display="block"; 
        document.getElementById('abs-chiller-g').style.display="none"; 
        document.getElementById('abs-chiller').style.display="block"; 
      }
      function removeTermStor(){
        document.getElementById('heatstor-g').style.display="none"; 
        document.getElementById('heatstor').style.display="block"; 
      }
      // function removeChp(){
      //   document.getElementById('chp-comp-g').style.display="none"; 
      //   document.getElementById('chp-comp').style.display="block"; 
      // }
   }



  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
  ];

  done = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];



  
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      let element = event.item.element.nativeElement.attributes[3].nodeValue
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
      // console.log(element)
       document.getElementById(element).style.display="none";
      let elementOnGraph = document.getElementById(element+'-g');
      elementOnGraph.style.display = "block";
      if(elementOnGraph.id == "chp-g"){
        document.getElementById('abs-chiller-g').style.display = "block"
      }
      if(elementOnGraph.id == "abs-chiller-g"){
        document.getElementById('chp-g').style.display = "block"
      }
       

    }
    
    
  }

}
