import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragdropDComponent } from './dragdrop-d.component';

describe('DragdropDComponent', () => {
  let component: DragdropDComponent;
  let fixture: ComponentFixture<DragdropDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragdropDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragdropDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
