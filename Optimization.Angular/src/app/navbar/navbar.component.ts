import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', () =>{
        $('#navbar').toggleClass('active');
        $('#sidebarCollapse').toggleClass('active');
        $('#row-dash').toggleClass('active');
        $('#search-bar').toggleClass('active');
        $('#welcome').toggleClass('activ');
        $('.btn-user').toggleClass('active');


      });


  
      document.getElementById('opt-sel').addEventListener('click',reload)
      document.getElementById('opt-res').addEventListener('click',reload)
      document.getElementById('inp-sel').addEventListener('click',reload)


      function reload(){
        // window.location.reload()

        $('#sidebarCollapse').toggleClass('active');
        $('#sidebarCollapse').toggleClass('coll-attivo');
 
      }


  
      let sidebar = document.getElementById('sidebar');

      if(sidebar.className == "active"){
          $('#master').on('click', function () {
          $('#sidebar').removeClass('active');
          $('#revolt-logo').addClass('active');
          // $('#overlay').toggleClass('attivoo');
          $('#sidebarCollapse').addClass('active');

          $('#sidebarCollapse').addClass('coll-attivo');
          $('#linea-logo').addClass('linea-attiva');
          $('#scritta-logo').addClass('scritta-attiva');
          
      });
      $('#scheduling').on('click', function () {
        $('#sidebar').removeClass('active');
        $('#revolt-logo').addClass('active');
        // $('#overlay').toggleClass('attivoo');
        $('#sidebarCollapse').addClass('active');

        $('#sidebarCollapse').addClass('coll-attivo');
        $('#linea-logo').addClass('linea-attiva');
        $('#scritta-logo').addClass('scritta-attiva');
        
    });
      }
  
    }
    )};

}
