import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {


  constructor() { 

  }

  public cardColor = '#212B46';

  


   
  ngOnInit() {
    document.getElementById('homeSel').style.fill = "white";
    document.getElementById('masterSel').style.fill = "#196eed";
    document.getElementById('schedSel').style.fill = "white";

    document.getElementById('build-sel').style.color = "white"
    document.getElementById('opt-sel').style.color = "white";
    document.getElementById('opt-res').style.color = "white";
    // document.getElementById('summary-tab').style.color = "#196eed";


	Highcharts.chart('elLoad', this.options);

  }


  public options: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: 'areaspline',
       zoomType: 'xy',
  },
  
  plotOptions: {
    areaspline: {
        marker: {
            enabled: false
        }
    }
},
  legend: {
    layout: 'horizontal',
    align: 'center',
    verticalAlign: 'bottom',
    itemStyle: {
      color: 'white',
      fontWeight: 'bold',

  }
  },

  
    title: {
      style: {
        color: 'white',
      },
       text: ''
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: 'time [min]',
    },
    


    categories: [
     '30',
     '60',
     '90',
    '120',
    '150',
    '180',
    '210',
    '240',
    '270',
    '300',
    '330',
    '360',
    '390',
    '420',
    '450',
    '480',
    '510',
    '540',
    '570',
    '600',
    '630',
    '660',
    '690',
    '720',
    '750',
    '780',
    '810',
    '840',
    '870',
    '900',
    '930',
    '960',
    '990',
    '1020',
    '1050',
    '1080',
    '1110',
    '1140',
    '1170',
    '1200',
    '1230',
    '1260',
    '1290',
    '1320',
    '1350',
    '1380',
    '1410',
    '1440',

    ]
    },

    yAxis: [{ //--- Primary yAxis
      gridLineWidth: 0.2,
    lineWidth: 1,
      title: {
          text: 'Energy [kW]'
      },

  },
 { //--- Secondary yAxis
lineWidth: 1,
      title: {
          text: 'Cooling [RT]'
      },
      opposite: true
  }
],

    exporting: {
      enabled: false
	},

	credits: {
    enabled:false
 },
	
    series: [{
  lineWidth:1.5,
  lineColor: 'yellow',
  color: {
    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
    stops: [
        [0, 'yellow'],
        [1, 'transparent']
          ],
  },
  
  name: 'Energy [kW]',
  yAxis: 0,
  data: [
    984,
    972,
    976,
    948,
    912,
    908,
    885,
    942,
    936,
    926,
    956,
    930,
    908,
    986,
    922,
    982,
    1130,
    1246,
    1330,
    1310,
    1354,
    1286,
    1406,
    1418,
    1442,
    1442,
    1372,
    1488,
    1456,
    1418,
    1420,
    1610,
    1422,
    1554,
    1382,
    1388,
    1278,
    1256,
    1184,
    1014,
    992,
    982,
    964,
    934,
    958,
    918,
    920,
    914,
    
    ]
  },
  {
    lineWidth:1.5,
  lineColor: '#196eed',
  color: {
    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
    stops: [
        [0, '#196eed'],
        [1, 'transparent']
          ],
  },
    name: 'Cooling [RT]',
    yAxis:1,
    data:[
      363,
      377,
      358,
      349,
      365,
      359,
      327,
      335,
      343,
      343,
      348,
      325,
      353,
      353,
      449,
      445,
      817,
      785,
      630,
      721,
      617,
      698,
      676,
      678,
      662,
      660,
      656,
      656,
      651,
      642,
      671,
      683,
      660,
      635,
      479,
      436,
      399,
      369,
      335,
      305,
      295,
      287,
      281,
      278,
      277,
      275,
      371,
      368,
      
      ]
  }
	

]

}



 


}