import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptGraphsComponent } from './opt-graphs.component';

describe('OptGraphsComponent', () => {
  let component: OptGraphsComponent;
  let fixture: ComponentFixture<OptGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
