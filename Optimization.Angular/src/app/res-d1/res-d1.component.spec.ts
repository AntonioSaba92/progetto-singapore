import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResD1Component } from './res-d1.component';

describe('ResD1Component', () => {
  let component: ResD1Component;
  let fixture: ComponentFixture<ResD1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResD1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResD1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
