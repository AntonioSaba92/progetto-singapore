


import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';



  

  @Component({
    selector: 'app-res-d1',
    templateUrl: './res-d1.component.html',
    styleUrls: ['./res-d1.component.scss']
  })
  export class ResD1Component implements OnInit {
  
    constructor() { }
  
    ngOnInit() {
  
      document.getElementById('dragdrop').addEventListener('click',showDrag);
  
      function showDrag(){
        document.getElementById('drag').style.display = "block";
        document.getElementById('e-load').style.display = "none";
        document.getElementById('c-load').style.display = "none";
        document.getElementById('npv').style.display = "none";
        document.getElementById('c-npv').style.display = "none";
  
        document.getElementById('dragdrop').className = "nav-link active nav-link-custom";
        document.getElementById('elect-load').className = "nav-link  nav-link-custom";
        document.getElementById('cool-load').className = "nav-link  nav-link-custom";
        document.getElementById('net-pv').className = "nav-link  nav-link-custom";
        document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";
  
        
        
  
  
      }
  
      document.getElementById('elect-load').addEventListener('click',showEl);
  
      function showEl(){
        document.getElementById('drag').style.display = "none";
        document.getElementById('e-load').style.display = "block";
        document.getElementById('c-load').style.display = "none";
        document.getElementById('npv').style.display = "none";
        document.getElementById('c-npv').style.display = "none";
  
        document.getElementById('elect-load').className = "nav-link active nav-link-custom";
        document.getElementById('cool-load').className = "nav-link  nav-link-custom";
        document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
        document.getElementById('net-pv').className = "nav-link  nav-link-custom";
        document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";
  
  
  
  
  
        
      }
      document.getElementById('cool-load').addEventListener('click',showCool);
  
      function showCool(){
        document.getElementById('drag').style.display = "none";
        document.getElementById('e-load').style.display = "none";
        document.getElementById('c-load').style.display = "block";
        document.getElementById('npv').style.display = "none";
        document.getElementById('c-npv').style.display = "none";
  
         document.getElementById('cool-load').className = "nav-link active nav-link-custom";
        document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
        document.getElementById('elect-load').className = "nav-link  nav-link-custom";
        document.getElementById('net-pv').className = "nav-link  nav-link-custom";
        document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";
  
  
  
      }
      document.getElementById('net-pv').addEventListener('click',showNpv);
  
  
  function showNpv(){
    document.getElementById('drag').style.display = "none";
    document.getElementById('e-load').style.display = "none";
    document.getElementById('c-load').style.display = "none";
    document.getElementById('npv').style.display = "block";
    document.getElementById('c-npv').style.display = "none";
  
     document.getElementById('cool-load').className = "nav-link  nav-link-custom";
    document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
    document.getElementById('elect-load').className = "nav-link  nav-link-custom";
    document.getElementById('net-pv').className = "nav-link active nav-link-custom";
    document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";
  
  }
  
  document.getElementById('c-net-pv').addEventListener('click',showCNpv);
  
  
  function showCNpv(){
    document.getElementById('drag').style.display = "none";
    document.getElementById('e-load').style.display = "none";
    document.getElementById('c-load').style.display = "none";
    document.getElementById('npv').style.display = "none";
    document.getElementById('c-npv').style.display = "block";
  
    document.getElementById('c-net-pv').style.display = "block";
     document.getElementById('cool-load').className = "nav-link  nav-link-custom";
    document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
    document.getElementById('elect-load').className = "nav-link  nav-link-custom";
    document.getElementById('net-pv').className = "nav-link  nav-link-custom";
    document.getElementById('c-net-pv').className = "nav-link active nav-link-custom";
  
  }
  
  
  
  
  Highcharts.chart('electric', this.options);
  Highcharts.chart('cooling', this.options2);
  Highcharts.chart('net', this.options3);
  Highcharts.chart('cnpv', this.options4);
  
  
  
      document.getElementById('homeSel').style.fill = "white";
      document.getElementById('masterSel').style.fill = "#196eed";
      document.getElementById('schedSel').style.fill = "white";
  
      document.getElementById('inp-sel').style.color = "white"
      document.getElementById('opt-sel').style.color = "white";
      document.getElementById('build-sel').style.color = "white";
      document.getElementById('opt-res').style.color = "#196eed";
  
    }
  
  
  // ------------- PER I GRAFICI-------------------------
  
  
  
  
  public cardColor = '#212B46';
  
  public options: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: 'areaspline',
       zoomType: 'xy',
  },
  
  plotOptions: {
    areaspline: {
        marker: {
            enabled: false
        }
    }
  },
  
  legend: {
    layout: 'horizontal',
    align: 'center',
    verticalAlign: 'bottom',
    itemStyle: {
      color: 'white',
      fontWeight: 'bold',
  
  }
  },
  
  
    title: {
      style: {
        color: 'white',
      },
       text: ''
    },
  
    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: 'time [hr]',
    },
    
  
  
    categories: [
      '0.5',
      '1',
      '1.5',
      '2',
      '2.5',
      '3',
      '3.5',
      '4',
      '4.5',
      '5',
      '5.5',
      '6',
      '6.5',
      '7',
      '7.5',
      '8',
      '8.5',
      '9',
     '9.5',
      '10',
      '10.5',
      '11',
      '11.5',
      '12',
      '12.5',
      '13',
      '13.5',
      '14',
      '14.5',
      '15',
      '15.5',
      '16',
      '16.5',
      '17',
      '17.5',
      '18',
      '18.5',
      '19',
      '19.5',
      '20',
      '20.5',
      '21',
      '21.5',
      '22',
      '22.5',
      '23',
      '23.5',
      '24',
      
    ]
    },
  
    yAxis: [{ //--- Primary yAxis
      gridLineWidth: 0.2,
    lineWidth: 1,
      title: {
          text: 'Energy [kW]'
      },
  
  },
  { //--- Secondary yAxis
    lineWidth: 1,
          title: {
              text: 'Price [$/kWh]',
         rotation: 270

           
          },
          opposite: true,
      }
  
  ],
  
    exporting: {
      enabled: false
  },
  
  credits: {
    enabled:false
  },
  
    series: [{
  lineWidth:3.5,
  color: 'white',
  // color: {
  //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
  //   stops: [
  //       [0, 'yellow'],
  //       [1, 'transparent']
  //         ],
  // },
  
  name: 'Pload',
  yAxis: 0,
  type: 'line',
  marker: {
    enabled: false
  },
  data: [
    1474.52,
    1456.8,
    1462.36,
    1419.96,
    1366.11,
    1406.76,
    1318.54,
    1405,
    1848.99,
    1386.62,
    1431.92,
    1391.25,
    1360.15,
    1477.15,
    1363.8,
    1450.33,
    2074.27,
    2215.61,
    1958.13,
    2113.01,
    1979.79,
    1870.56,
    2054.72,
    2068.54,
    2106.52,
    2106.9,
    2003.8,
    2170.87,
    2138.28,
    2083.94,
    2072.43,
    2347.35,
    2069.9,
    2272.2,
    2066.52,
    2288.38,
    2415.68,
    1882.67,
    1773.08,
    1515.09,
    1980.76,
    1464.56,
    1436.58,
    1391.08,
    1426.89,
    1366.53,
    1378.72,
    1780.04
    
  
    
    ]
  },
  {
    lineWidth:1.5,
  color: 'yellow',
  type: 'line',
  marker: {
    enabled: false
  },
    name: 'PGimp',
   
    data:[
      0,
      0,
      0,
      162.21,
      1366.1,
      1406.7,
      1318.5,
      1404.9,
      1848.9,
      159.02,
      179.19,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      593.52,
      906.71,
      524.44,
      585.66,
      3.39,
      994.32,
      18.31,
      240.72,
      490.05,
      530.89,
      480.59,
      10.44,
      426.83
      
      
      
  ]
  },
  
  {
    lineWidth:1.5,
  color: '#FF4500',
  // fillColor: '#FF4500',
  
  type: 'line',
  marker: {
  
    enabled: false
  },
    name: 'Ppv',
   
    data:[
  
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      7.19,
      28.77,
      111.35,
      342.12,
      448.07,
      447.31,
      447.31,
      447.31,
      140.77,
      276.94,
      445.78,
      336.22,
      336.22,
      336.22,
      271.73,
      212.01,
      210.62,
      210.62,
      210.62,
      135.31,
      28.77,
      7.19,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
      
      
      
          
      ]
      },
      {
        lineWidth:1.5,
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
              [0, '#DC143C'],
              [1, 'transparent']
                ],
        },
      // fillColor: '#FF4500',
      
      type: 'area',
      marker: {
  
        enabled: false
      },
        name: 'PChp1',
       
        data:[
          1328.09,
          1398.43,
          1302.96,
          1257.75,
          0,
          0,
          0,
          0,
          0,
          1227.6,
          1252.7,
          1137.1,
          1277.8,
          1277.8,
          1252.4,
          1108.2,
          1626.1,
          1768.2,
          1307.5,
          1665.6,
          1839.0,
          1593.6,
          1608.9,
          1732.3,
          1770.3,
          1770.6,
          1732.0,
          1958.8,
          1927.6,
          1873.3,
          1861.8,
          2000,
          2000,
          2000,
          1910.9,
          1694.8,
          1508.9,
          1358.2,
          1187.4,
          1036.6,
          986.43,
          946.24,
          916.09,
          901.02,
          896,
          885.95,
          1368.2,
          1353.2
          
     
          ]
          },
          {
            lineWidth:1.5,
            color: {
              linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
              stops: [
                  [0, '#B22222'],
                  [1, 'transparent']
                    ],
            },
  
          
          type: 'area',
          marker: {
      
            enabled: false
          },
            name: 'PChp2',
           
            data:[
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0
              
              
              
         
              ]
              },
              // {
              //   lineWidth:1.5,
      
              // color: {
              //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
              //   stops: [
              //       [0, '#00e500'],
              //       [1, 'transparent']
              //         ],
              // },
    
              // // fillColor: '#FF4500',
              
              // type: 'area',
              // marker: {
              
              //   enabled: false
              // },
  
              //   name: 'Pach1',
               
              //   data:[
              //     139.45,
              //     146.83,
              //     136.81,
              //     132.06,
              //     0,
              //     0,
              //     0,
              //     0,
              //     0,
              //     128.9,
              //     131.54,
              //     119.4,
              //     134.17,
              //     134.17,
              //     131.51,
              //     116.36,
              //     170.75,
              //     185.67,
              //     137.3,
              //     174.9,
              //     193.1,
              //     167.33,
              //     168.94,
              //     181.89,
              //     185.88,
              //     185.92,
              //     181.87,
              //     205.68,
              //     202.4,
              //     196.7,
              //     195.49,
              //     210,
              //     210,
              //     210,
              //     200.64,
              //     177.96,
              //     158.44,
              //     142.61,
              //     124.68,
              //     108.85,
              //     103.58,
              //     99.36,
              //     96.19,
              //     94.61,
              //     94.08,
              //     93.02,
              //     143.67,
              //     142.09
                  
                  
                      
              //     ]
              //     },
              //     {
              //       lineWidth:1.5,
          
              //     color: {
              //       linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
              //       stops: [
              //           [0, '#006600'],
              //           [1, 'transparent']
              //             ],
              //     },
        
              //     // fillColor: '#FF4500',
                  
              //     type: 'area',
              //     marker: {
                  
              //       enabled: false
              //     },
      
              //       name: 'Pach2',
                   
              //       data:[
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0,
              //         0
                      
                          
              //         ]
              //         },
              // {
              //   lineWidth:1.5,
      
              // color: {
              //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
              //   stops: [
              //       [0, '#8B0000'],
              //       [1, 'transparent']
              //         ],
              // },
    
              // // fillColor: '#FF4500',
              
              // type: 'area',
              // marker: {
              
              //   enabled: false
              // },
  
              //   name: 'Pvch1',
               
              //   data:[
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     179.78,
              //     159.02,
              //     137.24,
              //     142.68,
              //     148.13,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     114,
              //     126.73,
              //     244.57,
              //     179.78,
              //     179.78,
              //     179.78,
              //     126.73,
              //     173.88,
              //     156.82,
              //     141.46,
              //     178.46,
              //     177.05,
              //     179.56,
              //     126.73,
              //     179.78,
              //     179.78,
              //     179.78,
              //     161.63,
              //     126.73,
              //     128.95,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43,
              //     101.43
                  
                  
              //     ]
              //     },
                  // {
                  //   lineWidth:1.5,
          
                  // color: {
                  //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                  //   stops: [
                  //       [0, '#ff4108'],
                  //       [1, 'transparent']
                  //         ],
                  // },
        
                  // // fillColor: '#FF4500',
                  
                  // type: 'area',
                  // marker: {
                  
                  //   enabled: false
                  // },
      
                  //   name: 'Pvch2',
                   
                  //   data:[
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     163.11,
                  //     179.78,
                  //     179.78,
                  //     179.78,
                  //     179.78,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     126.73,
                  //     123.27,
                  //     244.57,
                  //     234.15,
                  //     166.34,
                  //     179.78,
                  //     138.51,
                  //     179.78,
                  //     179.78,
                  //     179.78,
                  //     126.73,
                  //     126.73,
                  //     126.73,
                  //     148.82,
                  //     110.15,
                  //     110.83,
                  //     122.56,
                  //     126.73,
                  //     145.97,
                  //     126.73,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43,
                  //     101.43
                      
                      
                  //     ]
                  //     },
                  {
                    lineWidth:1.5,
                    color: 'yellowgreen',
          
                  
                  type: 'histogram',
                  marker: {
              
                    enabled: false
                  },
                    name: 'Ebat1',
                    data:[
                      250,
                      144.94,
                      63.17,
                      63.17,
                      63.17,
                      86.45,
                      86.45,
                      86.45,
                      306.3,
                      306.3,
                      306.3,
                      176.0,
                      137.4,
                      50,
                      50,
                      50,
                      251.05,
                      450,
                      345.75,
                      450,
                      450,
                      450,
                      450,
                      450,
                      450,
                      450,
                      450,
                      450,
                      450,
                      450,
                      450,
                      341.23,
                      320.13,
                      184.18,
                      104.35,
                      206.33,
                      450,
                      450,
                      450,
                      206.32,
                      450,
                      193.51,
                      50,
                      50,
                      50,
                      50,
                      50,
                      250
                      
                 
                      ]
                      },
  
                      {
                        lineWidth:1.5,
              
                      color: 'red',
            
                      // fillColor: '#FF4500',
                      
                      type: 'line',
                      marker: {
                      
                        enabled: false
                      },
                      yAxis:1,
                      opposite: true,
                        name: 'pGr [$/kW]',
                        dashStyle: 'shortdot',
                       
  data:[
    0.1103,
    0.1054,
    0.1014,
    0.0948,
    0.0934,
    0.0931,
    0.0931,
    0.0931,
    0.0924,
    0.0931,
    0.094,
    0.1064,
    0.1118,
    0.1179,
    0.1029,
    0.1131,
    0.1167,
    0.118,
    0.1388,
    0.1547,
    0.2004,
    0.2178,
    0.2098,
    0.2033,
    0.2018,
    0.2033,
    0.2179,
    0.235,
    0.235,
    0.2178,
    0.2178,
    0.2178,
    0.2178,
    0.2076,
    0.1533,
    0.125,
    0.1209,
    0.1254,
    0.1388,
    0.1388,
    0.1285,
    0.1534,
    0.1428,
    0.1388,
    0.1182,
    0.1156,
    0.1102,
    0.1074
    
                              
  ]
  },  
  ]
  }
  
  public options2: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: 'areaspline',
       zoomType: 'xy',
  },
  
  plotOptions: {
    areaspline: {
        marker: {
            enabled: false
        }
    }
  },
  legend: {
    layout: 'horizontal',
    align: 'center',
    verticalAlign: 'bottom',
    itemStyle: {
      color: 'white',
      fontWeight: 'bold',
  
  }
  },
  
  
    title: {
      style: {
        color: 'white',
      },
       text: ''
    },
  
    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: 'time [hr]',
    },
    
  
  
    categories: [
      '0.5',
      '1',
      '1.5',
      '2',
      '2.5',
      '3',
      '3.5',
      '4',
      '4.5',
      '5',
      '5.5',
      '6',
      '6.5',
      '7',
      '7.5',
      '8',
      '8.5',
      '9',
     '9.5',
      '10',
      '10.5',
      '11',
      '11.5',
      '12',
      '12.5',
      '13',
      '13.5',
      '14',
      '14.5',
      '15',
      '15.5',
      '16',
      '16.5',
      '17',
      '17.5',
      '18',
      '18.5',
      '19',
      '19.5',
      '20',
      '20.5',
      '21',
      '21.5',
      '22',
      '22.5',
      '23',
      '23.5',
      '24',
      
    ]
    },
  
    yAxis: [{ //--- Primary yAxis
      gridLineWidth: 0.2,
    lineWidth: 1,
      title: {
          text: 'Energy [kW]'
      },
  
  },
  { //--- Secondary yAxis
    lineWidth: 1,
          title: {
              text: 'Price [$/kWh]',
         rotation: 270

          },
          opposite: true
      }
  
  ],
  
    exporting: {
      enabled: false
  },
  
  credits: {
    enabled:false
  },
  
    series: [{
  lineWidth:3.5,
  color: '#196eed',
  // color: {
  //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
  //   stops: [
  //       [0, 'yellow'],
  //       [1, 'transparent']
  //         ],
  // },
  
  name: 'Cload',
  yAxis: 0,
  type: 'line',
  marker: {
    enabled: false
  },
  data: [
    1915.01,
    1988.86,
    1888.63,
    1841.15,
    1925.56,
    1893.9,
    1725.09,
    1767.29,
    1809.5,
    1809.5,
    1835.87,
    1714.54,
    1862.25,
    1862.25,
    2368.7,
    2347.6,
    4310.08,
    4141.27,
    3323.57,
    3803.64,
    3254.98,
    3682.3,
    3566.24,
    3576.79,
    3492.38,
    3481.83,
    3460.73,
    3460.73,
    3434.35,
    3386.87,
    3539.86,
    3603.17,
    3481.83,
    3349.94,
    2526.96,
    2300.12,
    2104.92,
    1946.66,
    1767.29,
    1609.03,
    1556.27,
    1514.07,
    1482.42,
    1466.59,
    1461.31,
    1450.76,
    1957.21,
    1941.38
    
    
    
  
    ]
  },
  {
    lineWidth:1.5,
    color: {
      linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
      stops: [
          [0, 'green'],
          [1, 'transparent']
            ],
    },
  type: 'area',
  marker: {
    enabled: false
  },
    name: 'Qachl1',
   
    data:[
      1394.49,
      1468.35,
      1368.11,
      1320.63,
      0,
      0,
      0,
      0,
      0,
      1288.98,
      1315.36,
      1194.02,
      1341.74,
      1341.74,
      1315.08,
      1163.62,
      1707.5,
      1856.71,
      1372.97,
      1748.97,
      1930.97,
      1673.29,
      1689.39,
      1818.94,
      1858.83,
      1859.21,
      1818.68,
      2056.82,
      2024.04,
      1966.98,
      1954.89,
      2100,
      2100,
      2100,
      2006.45,
      1779.6,
      1584.41,
      1426.14,
      1246.78,
      1088.51,
      1035.76,
      993.55,
      961.9,
      946.07,
      940.8,
      930.25,
      1436.69,
      1420.87
      
  ]
  },
  
  {
    lineWidth:1.5,
  color: '#ADFF2F',
  // fillColor: '#FF4500',
  
  type: 'area',
  marker: {
  
    enabled: false
  },
    name: 'Qachl2',
   
    data:[
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0
      
      
          
      ]
      },
      {
        lineWidth:1.5,
        color: {
          linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
          stops: [
              [0, '#00FA9A'],
              [1, 'transparent']
                ],
        },
      // fillColor: '#FF4500',
      
      type: 'area',
      marker: {
  
        enabled: false
      },
        name: 'Qvch1',
       
        data:[
          260.26,
          260.26,
          260.26,
          260.26,
          1027.3,
          866.57,
          697.75,
          739.96,
          782.16,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          437.22,
          616.4,
          1301.2,
          1027.3,
          1027.3,
          1027.3,
          616.4,
          981.67,
          849.51,
          730.51,
          1017.1,
          1006.2,
          1025.6,
          616.4,
          1027.3,
          1027.3,
          1027.3,
          886.77,
          616.4,
          633.54,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26,
          260.26
          
          
     
          ]
          },
          {
            lineWidth:1.5,
            color: {
              linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
              stops: [
                  [0, '#00965c'],
                  [1, 'transparent']
                    ],
            },
          // fillColor: '#FF4500',
          
          type: 'area',
          marker: {
      
            enabled: false
          },
            name: 'Qvch2',
           
            data:[
              260.26,
              260.26,
              260.26,
              260.26,
              898.22,
              1027.3,
              1027.3,
              1027.3,
              1027.3,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              616.4,
              567.57,
              1301.2,
              1257.2,
              923.26,
              1027.3,
              707.61,
              1027.3,
              1027.3,
              1027.3,
              616.4,
              616.4,
              616.4,
              787.51,
              382.98,
              392.56,
              557.64,
              616.4,
              765.43,
              616.4,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26,
              260.26
              
              
              
         
              ]
              },
          
                  {
                    lineWidth:1.5,
                    color: '#00FFFF',
                    opacity:0.6,
                  
                  type: 'histogram',
                  marker: {
              
                    enabled: false
                  },
                    name: 'ETES [kWh]',
                   
                    data:[
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0
  
                      
                 
                      ]
                      },
  
                      {
                        yAxis:1,
                        lineWidth:1.5,
                        dashStyle: 'shortdot',
                      color: 'red',
                      type: 'line',
                      marker: {
                        enabled: false
                      },
                      opposite:true,
                        name: 'pGr',
                       
                        data:[
                          0.1103,
                          0.1054,
                          0.1014,
                          0.0948,
                          0.0934,
                          0.0931,
                          0.0931,
                          0.0931,
                          0.0924,
                          0.0931,
                          0.094,
                          0.1064,
                          0.1118,
                          0.1179,
                          0.1029,
                          0.1131,
                          0.1167,
                          0.118,
                          0.1388,
                          0.1547,
                          0.2004,
                          0.2178,
                          0.2098,
                          0.2033,
                          0.2018,
                          0.2033,
                          0.2179,
                          0.235,
                          0.235,
                          0.2178,
                          0.2178,
                          0.2178,
                          0.2178,
                          0.2076,
                          0.1533,
                          0.125,
                          0.1209,
                          0.1254,
                          0.1388,
                          0.1388,
                          0.1285,
                          0.1534,
                          0.1428,
                          0.1388,
                          0.1182,
                          0.1156,
                          0.1102,
                          0.1074
                          
                          
                      ]
                      }
                      
    
  ]
  }
  
  public options3: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: 'column',
       zoomType: 'xy',
  },
  
  
  legend: {
    layout: 'horizontal',
    align: 'center',
    verticalAlign: 'bottom',
    itemStyle: {
      color: 'white',
      fontWeight: 'bold',
  
  }
  },
  
  
    title: {
      style: {
        color: 'white',
      },
       text: ''
    },
  
    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: 'time [yrs]',
    },
  
  
  
    categories: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
    },
  
    yAxis: [{ //--- Primary yAxis
      gridLineWidth: 0.2,
      title: {
          text: 'NPV [$]'
      },
  
  },
  
  
  ],
  
    exporting: {
      enabled: false
  },
  
  credits: {
    enabled:false
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    color: 'yellow',
    name:'npv',
    data: [-4590500,
      -3698200,
      -2856400,
      -2062300,
      -1313100,
      -606400,
       60400 ,
       689400,
       1282800,
       1842600,
       2370800,
       2869000,
       3339100,
       3782500,
       4200800,
       4595500,
       4967800,
       5319000,
       5650400,
       5963000,
       6257900,
      ]
    }]
  
  }
  
  public options4: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: 'column',
       zoomType: 'xy',
  },
  
  
  legend: {
    layout: 'horizontal',
    align: 'center',
    verticalAlign: 'bottom',
    itemStyle: {
      color: 'white',
      fontWeight: 'bold',
  
  }
  },
  
  
    title: {
      style: {
        color: 'white',
      },
       text: ''
    },
  
    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: 'time [yrs]',
    },
  
  
  
    categories: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
    },
  
    yAxis: [{ //--- Primary yAxis
      gridLineWidth: 0.2,
      title: {
          text: 'NPV [$]'
      },
  
  },
  
  
  ],
  
    exporting: {
      enabled: false
  },
  
  credits: {
    enabled:false
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'Case 1',
    data: [-2523000,
      -1992700,
      -1492400,
      -1020400,
      -575200,
      -155100,
       241200,
       615000,
       967700,
       1300400,
       1614300,
       1910400,
       2189800,
       2453300,
       2702000,
       2936500,
       3157800,
       3366500,
       3563500,
       3749300,
       3924500]
  
  }, {
    name: 'Case 2',
    color: 'red',
    data: [-2929000,
      -1863000,
      -857000,
       92000,
       987000, 
       1831000,
       2627000,
       3379000,
       4088000,
       4757000,
       5387000,
       5983000,
       6544000,
       7074000,
       7574000,
       8045000,
       8490000,
       8909000,
       9305000,
       9679000,
       10031000
      ]
  
  }, {
    name: 'Case 3',
    color:'yellow',
    data: [-4590500,
      -3698200,
      -2856400,
      -2062300,
      -1313100,
      -606400,
       60400 ,
       689400,
       1282800,
       1842600,
       2370800,
       2869000,
       3339100,
       3782500,
       4200800,
       4595500,
       4967800,
       5319000,
       5650400,
       5963000,
       6257900,
      ]
  
  }, {
    name: 'Case 4',
    color: 'green',
    data: [-4271000,
      -2574000,
      -973000,
       537000,
       1962000,
       3307000,
       4575000,
       5771000,
       6900000,
       7965000,
       8969000,
       9917000,
       10811000,
       11654000,
       12450000,
       13200000,
       13908000,
       14576000,
       15207000,
       15801000,
       16362000]
  
  }]
  
  }
  
  
  }
  
  
  
  
  