import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResS2Component } from './res-s2.component';

describe('ResS2Component', () => {
  let component: ResS2Component;
  let fixture: ComponentFixture<ResS2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResS2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
