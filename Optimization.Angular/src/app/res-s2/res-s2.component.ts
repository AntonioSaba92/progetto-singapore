
import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-res-s2',
  templateUrl: './res-s2.component.html',
  styleUrls: ['./res-s2.component.scss']
})

export class ResS2Component implements OnInit {

  constructor() { }

  ngOnInit() {

    document.getElementById('dragdrop').addEventListener('click',showDrag);

    function showDrag(){
      document.getElementById('drag').style.display = "block";
      document.getElementById('e-load').style.display = "none";
      document.getElementById('c-load').style.display = "none";
      document.getElementById('npv').style.display = "none";
      document.getElementById('c-npv').style.display = "none";

      document.getElementById('dragdrop').className = "nav-link active nav-link-custom";
      document.getElementById('elect-load').className = "nav-link  nav-link-custom";
      document.getElementById('cool-load').className = "nav-link  nav-link-custom";
      document.getElementById('net-pv').className = "nav-link  nav-link-custom";
      document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";

      
      


    }

    document.getElementById('elect-load').addEventListener('click',showEl);

    function showEl(){
      document.getElementById('drag').style.display = "none";
      document.getElementById('e-load').style.display = "block";
      document.getElementById('c-load').style.display = "none";
      document.getElementById('npv').style.display = "none";
      document.getElementById('c-npv').style.display = "none";

      document.getElementById('elect-load').className = "nav-link active nav-link-custom";
      document.getElementById('cool-load').className = "nav-link  nav-link-custom";
      document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
      document.getElementById('net-pv').className = "nav-link  nav-link-custom";
      document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";





      
    }
    document.getElementById('cool-load').addEventListener('click',showCool);

    function showCool(){
      document.getElementById('drag').style.display = "none";
      document.getElementById('e-load').style.display = "none";
      document.getElementById('c-load').style.display = "block";
      document.getElementById('npv').style.display = "none";
      document.getElementById('c-npv').style.display = "none";

       document.getElementById('cool-load').className = "nav-link active nav-link-custom";
      document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
      document.getElementById('elect-load').className = "nav-link  nav-link-custom";
      document.getElementById('net-pv').className = "nav-link  nav-link-custom";
      document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";



    }
    document.getElementById('net-pv').addEventListener('click',showNpv);


function showNpv(){
  document.getElementById('drag').style.display = "none";
  document.getElementById('e-load').style.display = "none";
  document.getElementById('c-load').style.display = "none";
  document.getElementById('npv').style.display = "block";
  document.getElementById('c-npv').style.display = "none";

   document.getElementById('cool-load').className = "nav-link  nav-link-custom";
  document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
  document.getElementById('elect-load').className = "nav-link  nav-link-custom";
  document.getElementById('net-pv').className = "nav-link active nav-link-custom";
  document.getElementById('c-net-pv').className = "nav-link  nav-link-custom";

}

document.getElementById('c-net-pv').addEventListener('click',showCNpv);


function showCNpv(){
  document.getElementById('drag').style.display = "none";
  document.getElementById('e-load').style.display = "none";
  document.getElementById('c-load').style.display = "none";
  document.getElementById('npv').style.display = "none";
  document.getElementById('c-npv').style.display = "block";

  document.getElementById('c-net-pv').style.display = "block";
   document.getElementById('cool-load').className = "nav-link  nav-link-custom";
  document.getElementById('dragdrop').className = "nav-link  nav-link-custom";
  document.getElementById('elect-load').className = "nav-link  nav-link-custom";
  document.getElementById('net-pv').className = "nav-link  nav-link-custom";
  document.getElementById('c-net-pv').className = "nav-link active nav-link-custom";

}




Highcharts.chart('electric', this.options);
Highcharts.chart('cooling', this.options2);
Highcharts.chart('net', this.options3);
Highcharts.chart('cnpv', this.options4);



    document.getElementById('homeSel').style.fill = "white";
    document.getElementById('masterSel').style.fill = "#196eed";
    document.getElementById('schedSel').style.fill = "white";

    document.getElementById('inp-sel').style.color = "white"
    document.getElementById('opt-sel').style.color = "white";
    document.getElementById('build-sel').style.color = "white";
    document.getElementById('opt-res').style.color = "#196eed";

  }


// ------------- PER I GRAFICI-------------------------




public cardColor = '#212B46';

public options: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'areaspline',
     zoomType: 'xy',
},

plotOptions: {
  areaspline: {
      marker: {
          enabled: false
      }
  }
},

legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [hr]',
  },
  


  categories: [
    '0.5',
    '1',
    '1.5',
    '2',
    '2.5',
    '3',
    '3.5',
    '4',
    '4.5',
    '5',
    '5.5',
    '6',
    '6.5',
    '7',
    '7.5',
    '8',
    '8.5',
    '9',
   '9.5',
    '10',
    '10.5',
    '11',
    '11.5',
    '12',
    '12.5',
    '13',
    '13.5',
    '14',
    '14.5',
    '15',
    '15.5',
    '16',
    '16.5',
    '17',
    '17.5',
    '18',
    '18.5',
    '19',
    '19.5',
    '20',
    '20.5',
    '21',
    '21.5',
    '22',
    '22.5',
    '23',
    '23.5',
    '24',
    
  ]
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
  lineWidth: 1,
    title: {
        text: 'Energy [kW]'
    },

},
{ //--- Secondary yAxis
  lineWidth: 1,
        title: {
            text: 'Price [$/kWh]',
         rotation: 270

         
        },
        opposite: true,
    }

],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},

  series: [{
lineWidth:3.5,
color: 'white',
// color: {
//   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
//   stops: [
//       [0, 'yellow'],
//       [1, 'transparent']
//         ],
// },

name: 'Pload',
yAxis: 0,
type: 'line',
marker: {
  enabled: false
},
data: [
 
  1000.54,
  981.9,
  993.5,
  966.6,
  919.4,
  917.1,
  903.2,
  960.5,
  954.1,
  927.3,
  974.7,
  948,
  918.54,
  1000.12,
  921.73,
  965.8,
  1318.48,
  1425.92,
  1281.89,
  1271.94,
  1302.91,
  1263.7,
  1424.46,
  1359.09,
  1402.02,
  1420.15,
  1358.73,
  1429.24,
  1397.91,
  1361.07,
  1359.15,
  1547.37,
  1362.71,
  1497.91,
  1366.21,
  1582.2,
  1593.78,
  1275.61,
  1202.54,
  1030.55,
  1307.67,
  996.86,
  978.21,
  947.88,
  971.76,
  931.52,
  1000,
  1108.95
  
  
  ]
},
{
  lineWidth:1.5,
color: 'yellow',
type: 'line',
marker: {
  enabled: false
},
  name: 'PGimp',
 
  data:[
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    60.52,
    0,
    318.52,
    50.79,
    391.07,
    266.21,
    482.19,
    493.78,
    232.54,
    159.35,
    0,
    507.13,
    0,
    0,
    204.28,
    231.51,
    197.97,
    0,
    108.95
    
    
]
},

{
  lineWidth:1.5,
color: '#FF4500',
// fillColor: '#FF4500',

type: 'line',
marker: {

  enabled: false
},
  name: 'Ppv',
 
  data:[

    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    6.85,
    27.39,
    106.02,
    325.76,
    426.64,
    425.92,
    425.92,
    425.92,
    134.03,
    263.69,
    424.46,
    320.14,
    320.14,
    320.14,
    258.74,
    201.87,
    200.55,
    200.55,
    200.55,
    128.84,
    27.39,
    6.85,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
    
    
        
    ]
    },
    {
      lineWidth:1.5,
      color: {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        stops: [
            [0, '#DC143C'],
            [1, 'transparent']
              ],
      },
    // fillColor: '#FF4500',
    
    type: 'area',
    marker: {

      enabled: false
    },
      name: 'PChp1',
     
      data:[
        1000,
        981.89,
        993.5,
        966.65,
        919.41,
        917.16,
        903.25,
        934.52,
        954.15,
        793.84,
        974.75,
        901.02,
        911.68,
        945.8,
        815.7,
        640.04,
        891.84,
        1000,
        833.47,
        846.02,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        1000,
        934.52,
        834.03,
        800.54,
        773.74,
        753.64,
        743.59,
        740.24,
        733.55,
        1000,
        1000
        
   
        ]
        },
        {
          lineWidth:1.5,
          color: {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
                [0, '#B22222'],
                [1, 'transparent']
                  ],
          },

        
        type: 'area',
        marker: {
    
          enabled: false
        },
          name: 'PChp2',
         
          data:[
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            81.88,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            100,
            43.07,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
            
            
            
       
            ]
            },
            // {
            //   lineWidth:1.5,
    
            // color: {
            //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            //   stops: [
            //       [0, '#00e500'],
            //       [1, 'transparent']
            //         ],
            // },
  
            // // fillColor: '#FF4500',
            
            // type: 'area',
            // marker: {
            
            //   enabled: false
            // },

            //   name: 'Pach1',
             
            //   data:[
            //     105,
            //     103.1,
            //     104.32,
            //     101.5,
            //     96.54,
            //     96.3,
            //     94.84,
            //     98.12,
            //     100.19,
            //     83.35,
            //     102.35,
            //     94.61,
            //     95.73,
            //     99.31,
            //     85.65,
            //     67.2,
            //     93.64,
            //     105,
            //     87.51,
            //     88.83,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     105,
            //     98.12,
            //     87.57,
            //     84.06,
            //     81.24,
            //     79.13,
            //     78.08,
            //     77.73,
            //     77.02,
            //     105,
            //     105
                
                    
            //     ]
            //     },
            //     {
            //       lineWidth:1.5,
        
            //     color: {
            //       linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            //       stops: [
            //           [0, '#006600'],
            //           [1, 'transparent']
            //             ],
            //     },
      
            //     // fillColor: '#FF4500',
                
            //     type: 'area',
            //     marker: {
                
            //       enabled: false
            //     },
    
            //       name: 'Pach2',
                 
            //       data:[
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         9.66,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         11.8,
            //         5.08,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0,
            //         0
                    
                    
                        
            //         ]
            //         },
            // {
            //   lineWidth:1.5,
    
            // color: {
            //   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            //   stops: [
            //       [0, '#8B0000'],
            //       [1, 'transparent']
            //         ],
            // },
  
            // // fillColor: '#FF4500',
            
            // type: 'area',
            // marker: {
            
            //   enabled: false
            // },

            //   name: 'Pvch1',
             
            //   data:[
            //     140.73,
            //     140.73,
            //     140.73,
            //     141.8,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     175.04,
            //     175.95,
            //     316.02,
            //     264.2,
            //     211.24,
            //     249.67,
            //     183.61,
            //     219.29,
            //     209.6,
            //     210.48,
            //     191.33,
            //     187.78,
            //     186.01,
            //     186.01,
            //     183.81,
            //     179.85,
            //     192.62,
            //     197.91,
            //     187.78,
            //     176.76,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73,
            //     140.73
                
            //     ]
            //     },
                {
                  lineWidth:1.5,
                  color: 'yellowgreen',
        
                
                type: 'histogram',
                marker: {
            
                  enabled: false
                },
                  name: 'Ebat1',
                  data:[
                    150,
                    149.73,
                    149.73,
                    149.73,
                    149.73,
                    149.73,
                    149.73,
                    136.38,
                    136.38,
                    67.9,
                    67.9,
                    43.81,
                    43.81,
                    30,
                    30,
                    30,
                    147.42,
                    270,
                    258.46,
                    270,
                    183.37,
                    202.71,
                    240.28,
                    220.3,
                    229.58,
                    247.83,
                    270,
                    204.66,
                    154.72,
                    154.72,
                    124.65,
                    124.65,
                    30,
                    30,
                    30,
                    123.8,
                    270,
                    270,
                    214.26,
                    113.45,
                    259.65,
                    145.2,
                    30,
                    30,
                    30,
                    30,
                    62.24,
                    150
                    
                    
                    
               
                    ]
                    },

                    {
                      lineWidth:1.5,
            
                    color: 'red',
          
                    // fillColor: '#FF4500',
                    
                    type: 'line',
                    marker: {
                    
                      enabled: false
                    },
                    yAxis:1,
                    opposite: true,
                      name: 'pGr [$/kW]',
                      dashStyle: 'shortdot',
                     
data:[
  0.1522,
  0.1454,
  0.1398,
  0.1308,
  0.1288,
  0.1284,
  0.1284,
  0.1284,
  0.1274,
  0.1284,
  0.1296,
  0.1468,
  0.1542,
  0.1626,
  0.142,
  0.156,
  0.161,
  0.1628,
  0.1914,
  0.2134,
  0.2764,
  0.3004,
  0.2894,
  0.2804,
  0.2784,
  0.2804,
  0.3006,
  0.3242,
  0.3242,
  0.3004,
  0.3004,
  0.3004,
  0.3004,
  0.2864,
  0.2114,
  0.1724,
  0.1668,
  0.173,
  0.1914,
  0.1914,
  0.1772,
  0.2116,
  0.197,
  0.1914,
  0.163,
  0.1594,
  0.152,
  0.1482
  
  

                            
]
},  
]
}

public options2: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'areaspline',
     zoomType: 'xy',
},

plotOptions: {
  areaspline: {
      marker: {
          enabled: false
      }
  }
},
legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [hr]',
  },
  


  categories: [
    '0.5',
    '1',
    '1.5',
    '2',
    '2.5',
    '3',
    '3.5',
    '4',
    '4.5',
    '5',
    '5.5',
    '6',
    '6.5',
    '7',
    '7.5',
    '8',
    '8.5',
    '9',
   '9.5',
    '10',
    '10.5',
    '11',
    '11.5',
    '12',
    '12.5',
    '13',
    '13.5',
    '14',
    '14.5',
    '15',
    '15.5',
    '16',
    '16.5',
    '17',
    '17.5',
    '18',
    '18.5',
    '19',
    '19.5',
    '20',
    '20.5',
    '21',
    '21.5',
    '22',
    '22.5',
    '23',
    '23.5',
    '24',
    
  ]
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
  lineWidth: 1,
    title: {
        text: 'Energy [kW]'
    },

},
{ //--- Secondary yAxis
  lineWidth: 1,
        title: {
            text: 'Price [$/kWh]',
         rotation: 270

        },
        opposite: true
    }

],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},

  series: [{
lineWidth:3.5,
color: '#196eed',
// color: {
//   linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
//   stops: [
//       [0, 'yellow'],
//       [1, 'transparent']
//         ],
// },

name: 'Cload',
yAxis: 0,
type: 'line',
marker: {
  enabled: false
},
data: [
  1452.52,
  1501.76,
  1434.94,
  1227.43,
  1283.7,
  1262.6,
  1150.06,
  1178.19,
  1206.33,
  1206.33,
  1223.92,
  1318.87,
  1417.35,
  1417.35,
  1754.98,
  1661.91,
  2873.39,
  2760.84,
  2391.56,
  2535.76,
  2169.99,
  2607.81,
  2553.34,
  2384.53,
  2328.25,
  2321.22,
  2307.15,
  2307.15,
  2289.57,
  2257.91,
  2359.91,
  2402.11,
  2321.22,
  2233.3,
  1684.64,
  1584.38,
  1422.8,
  1473.62,
  1354.04,
  1248.53,
  1213.36,
  1185.23,
  1164.13,
  1153.58,
  1150.06,
  1143.02,
  1345.21,
  1339.35
  
  

  ]
},
{
  lineWidth:1.5,
  color: {
    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
    stops: [
        [0, 'green'],
        [1, 'transparent']
          ],
  },
type: 'area',
marker: {
  enabled: false
},
  name: 'Qachl1',
 
  data:[
    1050,
1038.33,
1044.6,
0,
0,
0,
0,
0,
0,
0,
0,
946.07,
965.89,
1034.4,
890.83,
778.95,
735.45,
908.92,
1026.1,
962.05,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
1050,
981.24,
875.73,
840.56,
812.43,
791.33,
780.77,
777.26,
770.22,
972.4,
966.55

]
},

{
  lineWidth:1.5,
color: '#ADFF2F',
// fillColor: '#FF4500',

type: 'area',
marker: {

  enabled: false
},
  name: 'Qachl2',
 
  data:[
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    98.08,
    43.62,
    43.62,
    43.62,
    118,
    86.22,
    98.21,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    118,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
    
    
        
    ]
    },
    {
      lineWidth:1.5,
      color: {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        stops: [
            [0, '#00FA9A'],
            [1, 'transparent']
              ],
      },
    // fillColor: '#FF4500',
    
    type: 'area',
    marker: {

      enabled: false
    },
      name: 'Qvch1',
     
      data:[
        402.52,
        463.43,
        390.34,
        1051.5,
        1283.7,
        1262.6,
        1150.0,
        1178.1,
        1206.3,
        1206.3,
        1166.6,
        372.8,
        451.46,
        382.87,
        864.15,
        882.95,
        1864.0,
        1730.2,
        1321.7,
        1471.5,
        882.95,
        1471.5,
        1405.1,
        1040.6,
        1114.5,
        977.37,
        963.3,
        963.3,
        945.72,
        914.06,
        1016.0,
        1058.2,
        977.37,
        889.45,
        516.64,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8,
        372.8
        
   
        ]
        },
        
                {
                  lineWidth:1.5,
                  color: '#00FFFF',
                  opacity:0.6,
                
                type: 'histogram',
                marker: {
            
                  enabled: false
                },
                  name: 'ETES [kWh]',
                 
                  data:[
                    527.55,
                    685.81,
                    764.95,
                    672.39,
                    672.39,
                    672.39,
                    672.39,
                    672.39,
                    672.39,
                    672.39,
                    642.24,
                    721.37,
                    800.5,
                    879.63,
                    958.77,
                    1002.3,
                    909.79,
                    868.69,
                    947.83,
                    917.04,
                    854.39,
                    923.21,
                    1002.3,
                    909.79,
                    885.73,
                    793.18,
                    700.62,
                    608.07,
                    515.52,
                    422.97,
                    330.41,
                    237.86,
                    145.31,
                    52.75,
                    52.75,
                    52.75,
                    61.54,
                    113.92,
                    193.06,
                    272.19,
                    351.32,
                    430.45,
                    509.59,
                    588.72,
                    667.85,
                    746.98,
                    765.16,
                    785.45
                    

                    
               
                    ]
                    },

                    {
                      yAxis:1,
                      lineWidth:1.5,
                      dashStyle: 'shortdot',
                    color: 'red',
                    type: 'line',
                    marker: {
                      enabled: false
                    },
                    opposite:true,
                      name: 'pGr',
                     
                      data:[
                        0.1522,
                        0.1454,
                        0.1398,
                        0.1308,
                        0.1288,
                        0.1284,
                        0.1284,
                        0.1284,
                        0.1274,
                        0.1284,
                        0.1296,
                        0.1468,
                        0.1542,
                        0.1626,
                        0.142,
                        0.156,
                        0.161,
                        0.1628,
                        0.1914,
                        0.2134,
                        0.2764,
                        0.3004,
                        0.2894,
                        0.2804,
                        0.2784,
                        0.2804,
                        0.3006,
                        0.3242,
                        0.3242,
                        0.3004,
                        0.3004,
                        0.3004,
                        0.3004,
                        0.2864,
                        0.2114,
                        0.1724,
                        0.1668,
                        0.173,
                        0.1914,
                        0.1914,
                        0.1772,
                        0.2116,
                        0.197,
                        0.1914,
                        0.163,
                        0.1594,
                        0.152,
                        0.1482
                        
                        
                    ]
                    }
                    
  
]
}

public options3: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'column',
     zoomType: 'xy',
},


legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [yrs]',
  },



  categories: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
    title: {
        text: 'NPV [$]'
    },

},


],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},
plotOptions: {
  column: {
    pointPadding: 0.2,
    borderWidth: 0
  }
},
series: [{
  color: 'red',
  name:'npv',
  data: [-2929000,
    -1863000,
    -857000,
     92000,
     987000, 
     1831000,
     2627000,
     3379000,
     4088000,
     4757000,
     5387000,
     5983000,
     6544000,
     7074000,
     7574000,
     8045000,
     8490000,
     8909000,
     9305000,
     9679000,
     10031000
    ]
  }]

}

public options4: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'column',
     zoomType: 'xy',
},


legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [yrs]',
  },



  categories: ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
    title: {
        text: 'NPV [$]'
    },

},


],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},
plotOptions: {
  column: {
    pointPadding: 0.2,
    borderWidth: 0
  }
},
series: [{
  name: 'Case 1',
  data: [-2523000,
    -1992700,
    -1492400,
    -1020400,
    -575200,
    -155100,
     241200,
     615000,
     967700,
     1300400,
     1614300,
     1910400,
     2189800,
     2453300,
     2702000,
     2936500,
     3157800,
     3366500,
     3563500,
     3749300,
     3924500]

}, {
  name: 'Case 2',
  color: 'red',
  data: [-2929000,
    -1863000,
    -857000,
     92000,
     987000, 
     1831000,
     2627000,
     3379000,
     4088000,
     4757000,
     5387000,
     5983000,
     6544000,
     7074000,
     7574000,
     8045000,
     8490000,
     8909000,
     9305000,
     9679000,
     10031000
    ]

}, {
  name: 'Case 3',
  color:'yellow',
  data: [-4590500,
    -3698200,
    -2856400,
    -2062300,
    -1313100,
    -606400,
     60400 ,
     689400,
     1282800,
     1842600,
     2370800,
     2869000,
     3339100,
     3782500,
     4200800,
     4595500,
     4967800,
     5319000,
     5650400,
     5963000,
     6257900,
    ]

}, {
  name: 'Case 4',
  color: 'green',
  data: [-4271000,
    -2574000,
    -973000,
     537000,
     1962000,
     3307000,
     4575000,
     5771000,
     6900000,
     7965000,
     8969000,
     9917000,
     10811000,
     11654000,
     12450000,
     13200000,
     13908000,
     14576000,
     15207000,
     15801000,
     16362000]

}]

}


}




