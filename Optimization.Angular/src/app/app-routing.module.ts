import { ReportComponent } from "./report/report.component";
import { DragdropD2Component } from "./dragdrop-d2/dragdrop-d2.component";
import { ResS2Component } from "./res-s2/res-s2.component";
import { DragdropDComponent } from "./dragdrop-d/dragdrop-d.component";
import { InputselDoubleComponent } from "./inputsel-double/inputsel-double.component";
import { DragdropSComponent } from "./dragdrop-s/dragdrop-s.component";
import { Dragdrop2Component } from "./dragdrop2/dragdrop2.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { MapDashboardComponent } from "./map-dashboard/map-dashboard.component";
import { PreviewComponent } from "./preview/preview.component";
import { DragDropComponent } from "./drag-drop/drag-drop.component";
import { InputSelComponent } from "./input-sel/input-sel.component";
import { DetailsComponent } from "./details/details.component";
import { OptimizationResultsComponent } from "./optimization-results/optimization-results.component";
import { InputSelSingleComponent } from "./input-sel-single/input-sel-single.component";
import { ResS1Component } from "./res-s1/res-s1.component";
import { ResD1Component } from "./res-d1/res-d1.component";
import { ResD2Component } from "./res-d2/res-d2.component";
import { DragdropS2Component } from "./dragdrop-s2/dragdrop-s2.component";
import { SummaryTableComponent } from "./summary-table/summary-table.component";

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "map-dashboard", component: MapDashboardComponent },
  { path: "preview", component: PreviewComponent },
  { path: "dragdrop", component: DragDropComponent },
  { path: "dragdrop2", component: Dragdrop2Component },
  { path: "inputsel", component: InputSelComponent },
  { path: "details", component: DetailsComponent },
  { path: "opt-results", component: OptimizationResultsComponent },
  { path: "inputsel-s", component: InputSelSingleComponent },
  { path: "dragdrop-s", component: DragdropSComponent },
  { path: "res-s1", component: ResS1Component },
  { path: "inputsel-d", component: InputselDoubleComponent },
  { path: "dragdrop-d", component: DragdropDComponent },
  { path: "res-d1", component: ResD1Component },
  { path: "res-s2", component: ResS2Component },
  { path: "res-d2", component: ResD2Component },
  { path: "dragdrop-d2", component: DragdropD2Component },
  { path: "dragdrop-s2", component: DragdropS2Component },
  { path: "summarytab", component: SummaryTableComponent },
  { path: "report", component: ReportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
