import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragdropS2Component } from './dragdrop-s2.component';

describe('DragdropS2Component', () => {
  let component: DragdropS2Component;
  let fixture: ComponentFixture<DragdropS2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragdropS2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragdropS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
