import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
declare var $:any;


@Component({
  selector: 'app-input-sel-single',
  templateUrl: './input-sel-single.component.html',
  styleUrls: ['./input-sel-single.component.scss']
})
export class InputSelSingleComponent implements OnInit {
  values = '';

onKey(event: any) { // without type info
  this.values = event.target.value;


    if(this.values <= '1.45'){
      document.getElementById('next-1').style.display = "block"
      document.getElementById('next-2').style.display = "none"


    }
    else {
      document.getElementById('next-1').style.display = "none"
      document.getElementById('next-2').style.display = "block"

    
  }

}



  constructor() { }

  ngOnInit() {

    document.getElementById('back-map').addEventListener('click',reload)
    function reload(){
      window.location.reload()
    }
Highcharts.chart('SolarProf', this.options);
Highcharts.chart('ElecProf', this.options2);




    document.getElementById('sol-in').addEventListener('click',upIm)
    function upIm(){
    document.getElementById('sol-btn').style.display = "none";
    document.getElementById('sol-prof').style.display = "block";
    document.getElementById('profile').style.display = "block";

  }
  document.getElementById('temp-in').addEventListener('click',upImTemp)
  function upImTemp(){
  document.getElementById('temp-btn').style.display = "none";
  document.getElementById('temp-prof').style.display = "block";

}

document.getElementById('hum-in').addEventListener('click',upImHum)
function upImHum(){
document.getElementById('hum-btn').style.display = "none";
document.getElementById('hum-prof').style.display = "block";

}

document.getElementById('elec-price-in').addEventListener('click',upImHElecPrice)
function upImHElecPrice(){
document.getElementById('elec-price-btn').style.display = "none";
document.getElementById('elec-price-prof').style.display = "block";
document.getElementById('elec-profile').style.display = "block";


}

document.getElementById('elec-cost-in').addEventListener('click',upImHElecCost)
function upImHElecCost(){
document.getElementById('elec-cost-btn').style.display = "none";
document.getElementById('elec-cost-prof').style.display = "block";

}

document.getElementById('wat-in').addEventListener('click',upImWat)
function upImWat(){
document.getElementById('wat-btn').style.display = "none";
document.getElementById('wat-prof').style.display = "block";

}

document.getElementById('wat-buy-in').addEventListener('click',upImWatBuy)
function upImWatBuy(){
document.getElementById('wat-buy-btn').style.display = "none";
document.getElementById('wat-buy-prof').style.display = "block";

}

document.getElementById('wat-sel-in').addEventListener('click',upImWatSel)
function upImWatSel(){
document.getElementById('wat-sel-btn').style.display = "none";
document.getElementById('wat-sel-prof').style.display = "block";

}

document.getElementById('fuel-in').addEventListener('click',upImFuel)
function upImFuel(){
document.getElementById('fuel-btn').style.display = "none";
document.getElementById('fuel-prof').style.display = "block";

}




    document.getElementById('homeSel').style.fill = "white";
    document.getElementById('masterSel').style.fill = "#196eed";
    document.getElementById('schedSel').style.fill = "white";


    // document.getElementById('summary-tab').style.color = "white"
    document.getElementById('build-sel').style.color = "white";
    document.getElementById('opt-res').style.color = "white";
    document.getElementById('opt-sel').style.color = "white";
    document.getElementById('inp-sel').style.color = "#196eed";


var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');
    
  slider.each(function(){

    value.each(function(){
      var value = $(this).prev().attr('value');
      $(this).html(value);
    });

    // range.on('input', function(){
    //   $(this).next(value).html(this.value);
    // });
  });
};

rangeSlider();
    
  }
  
  public cardColor = '#212B46';

  public options: any = {
    chart: {
      backgroundColor: this.cardColor,
      type: 'areaspline',
       zoomType: 'xy',
  },
  
  plotOptions: {
    areaspline: {
        marker: {
            enabled: false
        }
    }
},
  legend: {
    layout: 'horizontal',
    align: 'center',
    verticalAlign: 'bottom',
    itemStyle: {
      color: 'white',
      fontWeight: 'bold',

  }
  },

  
    title: {
      style: {
        color: 'white',
      },
       text: ''
    },

    xAxis: {
      gridLineWidth: 0.2,
      title: {
        text: 'time [min]',
    },
    


    categories: [
      '0.5',
      '1',
      '1.5',
      '2',
      '2.5',
      '3',
      '3.5',
      '4',
      '4.5',
      '5',
      '5.5',
      '6',
      '6.5',
      '7',
      '7.5',
      '8',
      '8.5',
      '9',
     '9.5',
      '10',
      '10.5',
      '11',
      '11.5',
      '12',
      '12.5',
      '13',
      '13.5',
      '14',
      '14.5',
      '15',
      '15.5',
      '16',
      '16.5',
      '17',
      '17.5',
      '18',
      '18.5',
      '19',
      '19.5',
      '20',
      '20.5',
      '21',
      '21.5',
      '22',
      '22.5',
      '23',
      '23.5',
      '24',

    ]
    },

    yAxis: [{ //--- Primary yAxis
      gridLineWidth: 0.2,
    lineWidth: 1,
      title: {
          text: 'DNI [W/m<sup>2</sup>]'
      },

  },

],

    exporting: {
      enabled: false
	},

	credits: {
    enabled:false
 },
	
    series: [{
  lineWidth:1.5,
  lineColor: '	#FF4500',
  color: {
    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
    stops: [
        [0, '	#FF4500'],
        [1, 'transparent']
          ],
  },
  
  name: 'Solar DNI',
  yAxis: 0,
  data: [
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    50.0,
    100.0,
    200.0,
    600.0,
    800.0,
    800.0,
    800.0,
    800.0,
    250.0,
    500.0,
    800.0,
    600.0,
    600.0,
    600.0,
    500.0,
    400.0,
    400.0,
    400.0,
    400.0,
    250.0,
    100.0,
    50.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    
    
    ]
  },


]

}

public options2: any = {
  chart: {
    backgroundColor: this.cardColor,
    type: 'areaspline',
     zoomType: 'xy',
},

plotOptions: {
  areaspline: {
      marker: {
          enabled: false
      }
  }
},
legend: {
  layout: 'horizontal',
  align: 'center',
  verticalAlign: 'bottom',
  itemStyle: {
    color: 'white',
    fontWeight: 'bold',

}
},


  title: {
    style: {
      color: 'white',
    },
     text: ''
  },

  xAxis: {
    gridLineWidth: 0.2,
    title: {
      text: 'time [min]',
  },
  


  categories: [
    '0.5',
    '1',
    '1.5',
    '2',
    '2.5',
    '3',
    '3.5',
    '4',
    '4.5',
    '5',
    '5.5',
    '6',
    '6.5',
    '7',
    '7.5',
    '8',
    '8.5',
    '9',
   '9.5',
    '10',
    '10.5',
    '11',
    '11.5',
    '12',
    '12.5',
    '13',
    '13.5',
    '14',
    '14.5',
    '15',
    '15.5',
    '16',
    '16.5',
    '17',
    '17.5',
    '18',
    '18.5',
    '19',
    '19.5',
    '20',
    '20.5',
    '21',
    '21.5',
    '22',
    '22.5',
    '23',
    '23.5',
    '24',

  ]
  },

  yAxis: [{ //--- Primary yAxis
    gridLineWidth: 0.2,
  lineWidth: 1,
    title: {
        text: 'Price Grid [$/kWh]'
    },

},

],

  exporting: {
    enabled: false
},

credits: {
  enabled:false
},

  series: [{
lineWidth:1.5,
lineColor: '	red',
color: {
  linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
  stops: [
      [0, '	red'],
      [1, 'transparent']
        ],
},

name: 'Price',
yAxis: 0,
data: [
  0.0761,
0.0727,
0.0699,
0.0654,
0.0644,
0.0642,
0.0642,
0.0642,
0.0637,
0.0642,
0.0648,
0.0734,
0.0771,
0.0813,
0.0710,
0.0780,
0.0805,
0.0814,
0.0957,
0.1067,
0.1382,
0.1502,
0.1447,
0.1402,
0.1392,
0.1402,
0.1503,
0.1621,
0.1621,
0.1502,
0.1502,
0.1502,
0.1502,
0.1432,
0.1057,
0.0862,
0.0834,
0.0865,
0.0957,
0.0957,
0.0886,
0.1058,
0.0985,
0.0957,
0.0815,
0.0797,
0.0760,
0.0741,

  
  
  ]
},


]

}
}
