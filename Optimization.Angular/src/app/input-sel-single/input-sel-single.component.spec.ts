import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSelSingleComponent } from './input-sel-single.component';

describe('InputSelSingleComponent', () => {
  let component: InputSelSingleComponent;
  let fixture: ComponentFixture<InputSelSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputSelSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSelSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
