import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragdropSComponent } from './dragdrop-s.component';

describe('DragdropSComponent', () => {
  let component: DragdropSComponent;
  let fixture: ComponentFixture<DragdropSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragdropSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragdropSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
