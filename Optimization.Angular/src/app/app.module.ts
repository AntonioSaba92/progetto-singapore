import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { ContentDashComponent } from "./content-dash/content-dash.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MapDashboardComponent } from "./map-dashboard/map-dashboard.component";
import { HttpClientModule } from "@angular/common/http";
import { NgDragDropModule } from "node_modules/ng-drag-drop";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { PreviewComponent } from "./preview/preview.component";
import { DragDropComponent } from "./drag-drop/drag-drop.component";
import { Dragdrop2Component } from "./dragdrop2/dragdrop2.component";
import { InputSelComponent } from "./input-sel/input-sel.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatNativeDateModule } from "@angular/material/core";
import { DetailsComponent } from "./details/details.component";
import { OptimizationResultsComponent } from "./optimization-results/optimization-results.component";
import { OptGraphsComponent } from "./opt-graphs/opt-graphs.component";
import { InputSelSingleComponent } from "./input-sel-single/input-sel-single.component";
import { DragdropSComponent } from "./dragdrop-s/dragdrop-s.component";
import { ResS1Component } from "./res-s1/res-s1.component";
import { InputselDoubleComponent } from "./inputsel-double/inputsel-double.component";
import { DragdropDComponent } from "./dragdrop-d/dragdrop-d.component";
import { ResD1Component } from "./res-d1/res-d1.component";
import { ResS2Component } from "./res-s2/res-s2.component";
import { ResD2Component } from "./res-d2/res-d2.component";
import { DragdropD2Component } from "./dragdrop-d2/dragdrop-d2.component";
import { DragdropS1Component } from "./dragdrop-s1/dragdrop-s1.component";
import { DragdropS2Component } from "./dragdrop-s2/dragdrop-s2.component";
import { SummaryTableComponent } from "./summary-table/summary-table.component";
import { ReportComponent } from './report/report.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    LoginComponent,
    DashboardComponent,
    NavbarComponent,
    ContentDashComponent,
    MapDashboardComponent,
    PreviewComponent,
    DragDropComponent,
    Dragdrop2Component,
    InputSelComponent,
    DetailsComponent,
    OptimizationResultsComponent,
    OptGraphsComponent,
    InputSelSingleComponent,
    DragdropSComponent,
    ResS1Component,
    InputselDoubleComponent,
    DragdropDComponent,
    ResD1Component,
    ResS2Component,
    ResD2Component,
    DragdropD2Component,
    DragdropS1Component,
    DragdropS2Component,
    SummaryTableComponent,
    ReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgDragDropModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
