import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";
import { Building } from "../building";

@Component({
  selector: "app-summary-table",
  templateUrl: "./summary-table.component.html",
  styleUrls: ["./summary-table.component.scss"]
})
export class SummaryTableComponent implements OnInit {
  nome: string;
  tipo: string;
  altezza: string;
  piani: string;

  building: Building[];

  constructor(private data: DataService, private location: Location) {}

  ngOnInit() {
    // this.data.currentName.subscribe(nome => this.nome = nome);
    // this.data.currentType.subscribe(tipo => this.tipo = tipo);
    // this.data.currentHeight.subscribe(altezza => this.altezza = altezza);
    // this.data.currentLevels.subscribe(piani => this.piani = piani);
    this.data.currentBuilding.subscribe(build => (this.building = build));
  }

  goBack() {
    this.location.back();
  }

  deleteRow(name) {
    for (let i = 0; i < this.building.length; ++i) {
      if (this.building[i].name === name) {
        this.building.splice(i, 1);
      }
    }
  }
}
