import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputselDoubleComponent } from './inputsel-double.component';

describe('InputselDoubleComponent', () => {
  let component: InputselDoubleComponent;
  let fixture: ComponentFixture<InputselDoubleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputselDoubleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputselDoubleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
