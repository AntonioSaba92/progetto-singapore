export class Building {
    name?: string;
    type?: string;
    height?: string;
    levels?: string;
}