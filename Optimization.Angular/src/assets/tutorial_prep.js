var ma = new OSMBuildings({
  position: { latitude: 52.519991, longitude: 13.406453 },
  zoom: 15,
  tilt: 30,
  state: false,
  minZoom: 14,
  maxZoom: 20,
  attribution:
    '© 3D <a href="https://osmbuildings.org/copyright/">OSM Buildings</a>'
});

ma.appendTo("map");

ma.addMapTiles(
  "https://{s}.tiles.mapbox.com/v3/osmbuildings.kbpalbpk/{z}/{x}/{y}.png",
  {
    attribution:
      '© Data <a href="https://openstreetmap.org/copyright/">OpenStreetMap</a> · © Map <a href="http://mapbox.com">Mapbox</a>'
  }
);

ma.addGeoJSONTiles(
  "https://{s}.data.osmbuildings.org/0.2/anonymous/tile/{z}/{x}/{y}.json"
);

//***************************************************************************

ma.on("pointerup", e => {
  if (e.target) {
    ma.highlight(e.target.id, "#f08000");
  } else {
    ma.highlight(null);
  }
});

//***************************************************************************
